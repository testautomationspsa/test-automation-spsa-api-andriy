package com.epam.utils.enums;

public enum  Frequency {
    NOT_REPEAT, DAILY, WEEKLY, MONTHLY, YEARLY, CUSTOM
}
