package com.epam.utils.enums;

public enum ResponseStatus {
    SUCCESS(200, "SUCCESS"), BAD_REQUEST(400, "BAD_REQUEST"),
    NOT_FOUND(404, "NOT_FOUND"), VALIDATION_MISTAKE(400, "VALIDATION_MISTAKE"),
    CREATED(201, "CREATED"),BAD_REQUEST_REQUEST_FP_NO_ADDRESS(400, "BAD_REQUEST"),
    FAIL(400,"FAIL");

    Integer responseCode;
    String responseStatus;

    ResponseStatus(Integer responseCode, String responseStatus) {
        this.responseCode = responseCode;
        this.responseStatus = responseStatus;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    @Override
    public String toString() {
        return responseStatus + "  " + responseCode;
    }
}
