package com.epam.utils.enums;

public enum  Confirmation {
    CONFIRMED, NOT_CONFIRMED, NOT_READ
}
