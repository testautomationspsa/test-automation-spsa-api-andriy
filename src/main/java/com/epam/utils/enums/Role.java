package com.epam.utils.enums;

public enum Role {
    USER("USER"), COACH("COACH"), VENUES_ADMIN("VENUES_ADMIN"), ADMIN("ADMIN");

    String roleString;

    Role(String roleString) {
        this.roleString = roleString;
    }

    public String getRoleString() {
        return roleString;
    }
}
