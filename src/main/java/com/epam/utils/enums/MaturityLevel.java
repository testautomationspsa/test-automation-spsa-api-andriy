package com.epam.utils.enums;

public enum MaturityLevel {
    BEGINNER, MIDDLE, PRO
}
