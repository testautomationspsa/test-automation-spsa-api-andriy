package com.epam.utils;

public class Constants {
    public static final String BASE_PATH = "https://sportbuddies.herokuapp.com/";
    public static final String LOGIN_PATH = "auth/login";
    public static final String PROFILE_PATH = "/profile";
    public static final String REQUESTS_PATH = "/requests";
    public static final String REQUESTS_POSSIBLE_PARTNERS_PATH = "/requests/possiblePartners";
    public static final String FIND_PARTNERS_PATH = "/requests/find-partners";
    public static final String ADDRESS_PATH = "/address";
    public static final String EVENTS_PATH = "/events";
    public static final String APPROVE_EVENT_PATH = "/approve-event";
    public static final String POSTS_PATH = "/posts";
    public static final String POSTS_FIND_BY_TAG_PATH = "/posts/findByTag";
    public static final String VENUE_PATH = "/venue";
    public static final String VENUE_ADMIN_PATH = "/venue/own/venues";
    public static final String VENUE_FIND_PATH = "/venue/find";
    public static final String ADMIN_PATH = "/admin/forms";
    public static final String ADMIN_GET_CONFIRMED_FORMS_PATH = "/admin/forms?confirmed=true";
    public static final String ADMIN_GET_UNCONFIRMED_FORMS_PATH = "/admin/forms?confirmed=false";
    public static final String ADMIN_UNREAD_PATH = "/admin/forms/unread";
    public static final String PERSON_INFO_PATH = "/person/info";
    public static final String TRAINING_RESULTS_PATH = "/training-results";
    public static final String TRAINING_RESULTS_PREVIOUS_DATA_PATH = "/training-results/previous-data";
    public static final String TRAINER_INFO_PATH = "/trainer-info";
    public static final String TRAINER_REQUEST_PATH = "/trainer-request";
    public static final String TRAINER_INFO_ALL_VENUES_PART = "/all/venues";
    public static final String TRAINER_INFO_APPROVED_REQUESTS_PART = "/approved/request";
    public static final String VENUE_GET_UNEMPLOYED_TRAINERS_PATH = "/check/trainers/request";
    public static final String VENUE_APPROVE_TRAINER_REQUEST_PATH = "/approve/trainer/request";
    public static final String VENUE_GET_ALL_TRAINERS_WHO_WORK_IN_PATH = "/trainers";
    public static final String CURRENT_PERSON_STATISTICS_PATH = "/currentPersonStatistics";
    public static final String TOTAL_TRAINING_STATISTICS_PATH = "/totalStatistics";
    public static final String WRONG_SPORT_TYPE_EXCEPTION = "sportType should be of type com.epam.sportbuddies.entity.enums.SportType";
    public static final String PERSON_HAS_NO_ADDRESS_EXCEPTION = "The person does not have a valid address";
    public static final Integer TEST_TRAINER_INFO_ID = 99999;
    public static final Integer TEST_USER_PERSON_ID = 1109;
    public static final Integer TEST_TRAINER_PERSON_ID = 13411;
    public static final Integer TEST_VENUE_ID = 1550;

    private Constants() {
    }
}
