package com.epam.dataproviders.dataparcer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CSVDataParser {
    private static final String DELIMITER = ",";

    private CSVDataParser() {
    }

    public static Iterator<Object[]> readFile(String path) {
        List<Object[]> data = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] values = line.split(DELIMITER);
                data.add(values);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data.stream().iterator();
    }
}
