package com.epam.dataproviders;

import com.epam.dataproviders.dataparcer.CSVDataParser;
import org.testng.annotations.DataProvider;

import java.util.Iterator;

public class DataProviders {
    private static final String PROFILE_TEST_DATAFILE_PATH = "./src/test/resources/testDataProfile.csv";
    private static final String REQUEST_VALIDATION_TEST_DATAFILE_PATH = "./src/test/resources/testDataValidationRequest.csv";
    private static final String REQUEST_TEST_DATAFILE_PATH = "./src/test/resources/testDataRequest.csv";
    private static final String ADDRESS_TEST_DATAFILE_PATH = "./src/test/resources/testDataAddress.csv";
    private static final String VENUE_TEST_DATAFILE_PATH = "./src/test/resources/testDataVenue.csv";
    private static final String POST_TEST_DATAFILE_PATH = "./src/test/resources/testDataPost.csv";
    private static final String FIND_POST_TEST_DATAFILE_PATH = "./src/test/resources/testDataFindPost.csv";
    private static final String AUTHENTICATION_TEST_DATAFILE_PATH = "./src/test/resources/testDataAuth.csv";
    private static final String FORMS_TEST_DATAFILE_PATH = "./src/test/resources/testDataForms.csv";
    private static final String TRAINING_RESULTS_TEST_DATAFILE_PATH = "./src/test/resources/testDataTrainingResults.csv";
    private static final String TRAINER_INFO_TEST_DATAFILE_PATH = "./src/test/resources/testDataTrainerInfo.csv";
    private static final String EVENTS_TEST_DATAFILE_PATH = "./src/test/resources/testDataEvents.csv";

    private DataProviders() {
    }

    @DataProvider
    public static Iterator<Object[]> saveProfileData() {
        return CSVDataParser.readFile(PROFILE_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> findPartnersValidationData() {
        return CSVDataParser.readFile(REQUEST_VALIDATION_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> findPartnersData() {
        return CSVDataParser.readFile(REQUEST_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> addressData() {
        return CSVDataParser.readFile(ADDRESS_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> venueData() {
        return CSVDataParser.readFile(VENUE_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> findPostData() {
        return CSVDataParser.readFile(FIND_POST_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> postData() {
        return CSVDataParser.readFile(POST_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> authData() {
        return CSVDataParser.readFile(AUTHENTICATION_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> formsData() {
        return CSVDataParser.readFile(FORMS_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> TrainingResultsData() {
        return CSVDataParser.readFile(TRAINING_RESULTS_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> trainerInfoData() {
        return CSVDataParser.readFile(TRAINER_INFO_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> eventsData() {
        return CSVDataParser.readFile(EVENTS_TEST_DATAFILE_PATH);
    }
}
