package com.epam.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder(toBuilder = true)
public class TrainingResultsModel extends BaseModel {
    public static final TrainingResultsModel CORRECT_TEST_TRAINING_RESULTS_MODEL =
        TrainingResultsModel.builder()
            .date("2001-01-01")
            .result(5.6f)
            .sportType("RUNNING")
            .trainer("testTrainer")
            .venue("testVenue").build();
    @EqualsAndHashCode.Exclude
    private Integer id;
    private String date;
    private Float result;
    private String sportType;
    private String trainer;
    private String venue;
}
