package com.epam.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder(toBuilder = true)
public class PostModel extends BaseModel {
    public static final PostModel CORRECT_TEST_POST_MODEL =
            PostModel.builder()
                    .name("Test name")
                    .text("Test text.")
                    .tags(new String[]{"testTag1", "testTag2", "testTag3"}).build();
    @EqualsAndHashCode.Exclude
    private Integer id;
    private String name;
    @EqualsAndHashCode.Exclude
    private String[] tags;
    private String text;
    @EqualsAndHashCode.Exclude
    private String[] pictures;
}
