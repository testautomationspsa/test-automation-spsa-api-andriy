package com.epam.models.submodels;

import com.epam.models.BaseModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class AddressModel extends BaseModel {
    public static final AddressModel CORRECT_TEST_ADDRESS_MODEL = AddressModel.builder()
            .country("Ukraine")
            .city("Lviv")
            .latitude(49.842957)
            .longitude(24.031111)
            .street("Shevchenka Street")
            .streetNumber("111").build();
    private String city;
    private String country;
    private Double latitude;
    private Double longitude;
    private String street;
    private String streetNumber;
}
