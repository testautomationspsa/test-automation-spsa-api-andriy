package com.epam.models.submodels;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GetPageSizeModel {
    public static final GetPageSizeModel CORRECT_PAGE_SIZE_VALUES =
            new GetPageSizeModel(5, 0);
    private Integer size;
    private Integer page;

    @Override
    public String toString() {
        return "Page=" + page + "&Size=" + size;
    }
}
