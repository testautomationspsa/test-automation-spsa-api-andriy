package com.epam.models;

import lombok.Data;

@Data
public class TotalTrainingStatisticsModel {
    private Double totalRunningDistance;
    private Double totalSwimmingDistance;
    private Double totalHoursByYoga;
    private Double totalHoursByFootball;
    private Integer totalUsers;
}
