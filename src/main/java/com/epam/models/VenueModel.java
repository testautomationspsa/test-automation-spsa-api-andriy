package com.epam.models;

import com.epam.models.submodels.AddressModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;

@Data
@Builder(toBuilder = true)
public class VenueModel extends BaseModel {
    public static final VenueModel CORRECT_TEST_VENUE_MODEL =
            VenueModel.builder()
                    .name("TestVenue")
                    .description("Test description.")
                    .inOut("INDOOR")
                    .sportType(Arrays.asList("RUNNING"))
                    .address(AddressModel.CORRECT_TEST_ADDRESS_MODEL)
                    .openTime("08:00")
                    .closeTime("18:00")
                    .phoneNumber(Arrays.asList("+380111111111")).build();
    public static final VenueModel CORRECT_TEST_VENUE_FIND_MODEL =
            VenueModel.builder()
                    .id(0)
                    .distance(10)
                    .name("")
                    .description("")
                    .inOut("INDOOR")
                    .sportType(Arrays.asList("RUNNING"))
                    .address(AddressModel.CORRECT_TEST_ADDRESS_MODEL)
                    .phoneNumber(Arrays.asList("+380111111111")).build();
    @EqualsAndHashCode.Exclude
    private Integer id;
    @EqualsAndHashCode.Exclude
    private Integer distance;
    private String description;
    private String inOut;
    private String name;
    @EqualsAndHashCode.Exclude
    private List<String> phoneNumber;
    @EqualsAndHashCode.Exclude
    private List<String> sportType;
    private AddressModel address;
    private String openTime;
    private String closeTime;


    @Override
    public String toString() {
        return "?" +
                "distance=" + distance +
                "&description='" + description + '\'' +
                "&inOut=" + inOut +
                "&name=" + name +
                "&sportType=" + sportType.get(0) +
                "&phoneNumber=" + phoneNumber.get(0) +
                "&address.country=" + address.getCountry() +
                "&address.city=" + address.getCity() +
                "&address.street=" + address.getStreet() +
                "&address.country=" + address.getCountry() +
                "&address.latitude=" + address.getLatitude() +
                "&address.longitude=" + address.getLongitude();
    }
}
