package com.epam.models;

import lombok.Data;

import java.util.List;

@Data
public class AllVenuesandTrainersModel extends BaseModel {
    private List<String> venues;
    private List<String> trainers;
}
