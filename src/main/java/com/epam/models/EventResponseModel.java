package com.epam.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(toBuilder = true)
public class EventResponseModel extends BaseModel {
    private Integer id;
    private List<SimplifiedProfileModel> attendees;
    private String beginTime;
    private String endTime;
    private String createDate;
    private String updateDate;
    private String description;
    private String frequency;
    private String title;
    private SimplifiedProfileModel owner;
}
