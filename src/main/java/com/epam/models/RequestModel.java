package com.epam.models;

import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;

@Data
@Builder(toBuilder = true)
public class RequestModel extends BaseModel {
    public static final RequestModel CORRECT_TEST_REQUEST_MODEL =
            RequestModel.builder()
                    .distance(10)
                    .gender(Gender.FEMALE)
                    .maturityLevel(MaturityLevel.MIDDLE)
                    .morning(false)
                    .afternoon(true)
                    .evening(true)
                    .minRunningDistance(1)
                    .maxRunningDistance(11)
                    .sportType(SportType.RUNNING).build();
    private Boolean morning;
    private Boolean afternoon;
    private Boolean evening;
    @EqualsAndHashCode.Exclude
    private Integer distance;
    private Gender gender;
    @EqualsAndHashCode.Exclude
    private Integer id;
    private MaturityLevel maturityLevel;
    private Integer minRunningDistance;
    private Integer maxRunningDistance;
    private SportType sportType;

    @Override
    public String toString() {
        return "?afternoon=" + (Objects.isNull(afternoon) ? "" : afternoon) +
                "&distance=" + (Objects.isNull(distance) ? "" : distance) +
                "&evening=" + (Objects.isNull(evening) ? "" : evening) +
                "&gender=" + (Objects.isNull(gender) ? "" : gender) +
                "&maturityLevel=" + (Objects.isNull(maturityLevel) ? "" : maturityLevel) +
                "&maxRunningDistance=" + (Objects.isNull(maxRunningDistance) ? "" : maxRunningDistance) +
                "&minRunningDistance=" + (Objects.isNull(minRunningDistance) ? "" : minRunningDistance) +
                "&morning=" + (Objects.isNull(morning) ? "" : morning) +
                "&sportType=" + (Objects.isNull(sportType) ? "" : sportType);
    }
}
