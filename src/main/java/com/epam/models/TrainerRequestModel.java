package com.epam.models;

import lombok.Data;

@Data
public class TrainerRequestModel extends BaseModel {
    private Integer id;
    private String confirmDate;
    private Boolean confirmed;
    private String createDate;
    private TrainerInfoModel trainerInfo;
    private VenueModel venueResponse;
}
