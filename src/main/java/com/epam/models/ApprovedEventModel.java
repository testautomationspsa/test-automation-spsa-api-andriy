package com.epam.models;

import lombok.Data;

@Data
public class ApprovedEventModel extends BaseModel {
    private Integer id;
    private String approveDate;
    private String confirmation;
    private String createDate;
    private EventResponseModel event;
}
