package com.epam.models;

import lombok.Data;

@Data
public class PartnerModel extends BaseModel {
    private Integer id;
    private String partnerName;
    private String partnerGender;
    private String partnerPhotoName;
    private String partnerCity;
    private String sportType;
    private Integer minRunningDistance;
    private Integer maxRunningDistance;
    private String maturityLevel;
    private Boolean morning;
    private Boolean afternoon;
    private Boolean evening;
}
