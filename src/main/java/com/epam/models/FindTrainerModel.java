package com.epam.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class FindTrainerModel extends BaseModel {
    public static final FindTrainerModel CORRECT_FIND_TRAINER_MODEL =
            FindTrainerModel.builder()
                    .sportType("RUNNING")
                    .gender("ANY").build();
    private String cityName;
    private String gender;
    private String sportType;
    private String venueName;

    @Override
    public String toString() {
        String query = "?";
        query += (cityName == null) ? "" : "cityName=" + cityName+"&";
        query += (gender == null) ? "" : "gender=" + gender+"&";
        query += (sportType == null) ? "" : "sportType=" + sportType+"&";
        query += (venueName == null) ? "" : "venueName=" + venueName+"&";
        return query;
    }
}
