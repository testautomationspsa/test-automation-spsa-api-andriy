package com.epam.models;

import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder(toBuilder = true)
public class PersonInfoModel extends BaseModel {
    public static final PersonInfoModel CORRECT_PERSON_INFO_MODEL =
            PersonInfoModel.builder()
            .height(182.)
            .maturityLevel(MaturityLevel.BEGINNER.toString())
            .sportType(SportType.RUNNING.toString())
            .weight(75.).build();
    @EqualsAndHashCode.Exclude
    private Double height;
    private String maturityLevel;
    private String sportType;
    @EqualsAndHashCode.Exclude
    private Double weight;
}
