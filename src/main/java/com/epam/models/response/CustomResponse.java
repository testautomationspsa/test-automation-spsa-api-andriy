package com.epam.models.response;

import com.epam.models.*;
import com.epam.models.submodels.AddressModel;
import com.epam.utils.enums.ResponseStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;
import org.testng.Assert;

import java.lang.reflect.Type;
import java.util.List;

public class CustomResponse {
    private ValidatableResponse response;

    public CustomResponse(ValidatableResponse response) {
        this.response = response;
    }

    public String getBody() {
        return response.extract().response().getBody().asString();
    }

    @Step("Verifying status code")
    public CustomResponse verifyStatusCode(ResponseStatus responseStatus) {
        response.statusCode(responseStatus.getResponseCode());
        return this;
    }

    @Step("Verifying Response Status")
    public CustomResponse verifyStatus(ResponseStatus responseStatus) {
        Type responseType = new TypeToken<ResponseModel<DummyModel>>() {
        }.getType();
        ResponseModel<DummyModel> generalizedModel = getResponseModelFromJSON(getBody(), responseType);
        Assert.assertEquals(generalizedModel.getStatus(), responseStatus.getResponseStatus(),
                "Response status is incorrect.");
        return this;
    }

    @Step("Verifying message")
    public CustomResponse verifyMessage(String text) {
        Assert.assertTrue(getBody().contains(text), "Message is incorrect.");
        return this;
    }

    public AddressModel getResponseAddressModel() {
        Type responseType = new TypeToken<ResponseModel<AddressModel>>() {
        }.getType();
        ResponseModel<AddressModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public RequestModel getResponseRequestModel() {
        Type responseType = new TypeToken<ResponseModel<RequestModel>>() {
        }.getType();
        ResponseModel<RequestModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public PostModel getResponsePostModel() {
        Type responseType = new TypeToken<ResponseModel<PostModel>>() {
        }.getType();
        ResponseModel<PostModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public ProfileModel getResponseProfileModel() {
        Type responseType = new TypeToken<ResponseModel<ProfileModel>>() {
        }.getType();
        ResponseModel<ProfileModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public VenueModel getResponseVenueModel() {
        Type responseType = new TypeToken<ResponseModel<VenueModel>>() {
        }.getType();
        ResponseModel<VenueModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public AllVenuesandTrainersModel getResponseAllVenuesandTrainersModel() {
        Type responseType = new TypeToken<ResponseModel<AllVenuesandTrainersModel>>() {
        }.getType();
        ResponseModel<AllVenuesandTrainersModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public EntitlementFormModel getResponseEntitlementModel() {
        Type responseType = new TypeToken<ResponseModel<EntitlementFormModel>>() {
        }.getType();
        ResponseModel<EntitlementFormModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public PersonInfoModel getResponsePersonInfoModel() {
        Type responseType = new TypeToken<ResponseModel<PersonInfoModel>>() {
        }.getType();
        ResponseModel<PersonInfoModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public Boolean getWasTrainingTodayValue() {
        Type responseType = new TypeToken<ResponseModel<Boolean>>() {
        }.getType();
        ResponseModel<Boolean> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public TrainerInfoModel getResponseTrainerInfoModel() {
        Type responseType = new TypeToken<ResponseModel<TrainerInfoModel>>() {
        }.getType();
        ResponseModel<TrainerInfoModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public TrainerFeedbackModel getResponseTrainerFeedbackModel() {
        Type responseType = new TypeToken<ResponseModel<TrainerFeedbackModel>>() {
        }.getType();
        ResponseModel<TrainerFeedbackModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public TrainingResultsModel getResponseTrainingResultsModel() {
        Type responseType = new TypeToken<ResponseModel<TrainingResultsModel>>() {
        }.getType();
        ResponseModel<TrainingResultsModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public TrainerRequestModel getResponseTrainerRequestModel() {
        Type responseType = new TypeToken<ResponseModel<TrainerRequestModel>>() {
        }.getType();
        ResponseModel<TrainerRequestModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public UserTrainingStatisticsModel getResponseUserTrainingStatisticsModel() {
        Type responseType = new TypeToken<ResponseModel<UserTrainingStatisticsModel>>() {
        }.getType();
        ResponseModel<UserTrainingStatisticsModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public TotalTrainingStatisticsModel getResponseTotalTrainingStatisticsModel() {
        Type responseType = new TypeToken<ResponseModel<TotalTrainingStatisticsModel>>() {
        }.getType();
        ResponseModel<TotalTrainingStatisticsModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public EventResponseModel getResponseEventResponseModel() {
        Type responseType = new TypeToken<ResponseModel<EventResponseModel>>() {
        }.getType();
        ResponseModel<EventResponseModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public ApprovedEventModel getResponseApprovedEventModel() {
        Type responseType = new TypeToken<ResponseModel<ApprovedEventModel>>() {
        }.getType();
        ResponseModel<ApprovedEventModel> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public List<TrainerFeedbackModel> getResponseTrainerFeedbackListModel() {
        Type responseType = new TypeToken<ResponseModel<List<TrainerFeedbackModel>>>() {
        }.getType();
        ResponseModel<List<TrainerFeedbackModel>> responseModel = getResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody();
    }

    public List<PartnerModel> getLongResponsePartnerModelList() {
        Type responseType = new TypeToken<FindPartnersListResponseModel>() {
        }.getType();
        FindPartnersListResponseModel responseModel = getLongResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody().getData();
    }

    public List<TrainerRequestModel> getLongResponseTrainerRequestModelList() {
        Type responseType = new TypeToken<TrainerOwnRequestResponseModel>() {
        }.getType();
        TrainerOwnRequestResponseModel responseModel = getLongTrainerOwnRequestModelFromJSON(getBody(), responseType);
        return responseModel.getBody().getData();
    }

    public List<SimplifiedProfileModel> getLongResponseFindPersonsForEventList() {
        Type responseType = new TypeToken<FindPersonsForEventListResponseModel>() {
        }.getType();
        FindPersonsForEventListResponseModel responseModel = getLongFindPersonsForEventListResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody().getData();
    }

    public List<EventResponseModel> getLongEventList() {
        Type responseType = new TypeToken<EventListResponseModel>() {
        }.getType();
        EventListResponseModel responseModel = getLongEventListResponseModelFromJSON(getBody(), responseType);
        return responseModel.getBody().getData();
    }

    private ResponseModel getResponseModelFromJSON(String JSONString, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(JSONString, type);
    }

    private FindPartnersListResponseModel getLongResponseModelFromJSON(String JSONString, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(JSONString, type);
    }

    private TrainerOwnRequestResponseModel getLongTrainerOwnRequestModelFromJSON(String JSONString, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(JSONString, type);
    }

    private FindPersonsForEventListResponseModel getLongFindPersonsForEventListResponseModelFromJSON(String JSONString, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(JSONString, type);
    }

    private EventListResponseModel getLongEventListResponseModelFromJSON(String JSONString, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(JSONString, type);
    }

    private class DummyModel {
    }
}
