package com.epam.models.response;

import com.epam.models.BaseModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseModel<T> extends BaseModel {
    private String status;
    private T body;
    private String message;
    private String detail;
    private String dateTime;
}
