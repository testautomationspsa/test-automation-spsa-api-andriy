package com.epam.models.response;

import com.epam.models.BaseModel;
import com.epam.models.SimplifiedProfileModel;
import lombok.Data;

import java.util.List;

@Data
public class FindPersonsForEventListResponseModel extends BaseModel {
    private String status;
    private FindPersonsForEventListResponseModel.SubResponse body;
    private String message;
    private String detail;
    private String dateTime;

    @Data
    public class SubResponse {
        private Integer totalPages;
        private Integer totalElements;
        private List<SimplifiedProfileModel> data;
    }
}
