package com.epam.models.response;

import com.epam.models.BaseModel;
import com.epam.models.TrainerRequestModel;
import lombok.Data;

import java.util.List;

@Data
public class TrainerOwnRequestResponseModel extends BaseModel {
    private String status;
    private TrainerOwnRequestResponseModel.SubResponse body;
    private String message;
    private String detail;
    private String dateTime;

    @Data
    public class SubResponse {
        private Integer totalPages;
        private Integer totalElements;
        private List<TrainerRequestModel> data;
    }
}
