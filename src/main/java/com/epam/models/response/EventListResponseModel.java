package com.epam.models.response;

import com.epam.models.BaseModel;
import com.epam.models.EventResponseModel;
import lombok.Data;

import java.util.List;

@Data
public class EventListResponseModel extends BaseModel {
    private String status;
    private EventListResponseModel.SubResponse body;
    private String message;
    private String detail;
    private String dateTime;

    @Data
    public class SubResponse {
        private Integer totalPages;
        private Integer totalElements;
        private List<EventResponseModel> data;
    }
}
