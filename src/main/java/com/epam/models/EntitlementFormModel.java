package com.epam.models;

import com.epam.utils.enums.Role;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder(toBuilder = true)
public class EntitlementFormModel extends BaseModel {
    public static final EntitlementFormModel CORRECT_TEST_ENTITLEMENT_MODEL =
            EntitlementFormModel.builder()
                    .firstName("TestFirstName")
                    .secondName("TestSecondName")
                    .email("test@gmail.com")
                    .cellphone("+380111111111")
                    .role(Role.VENUES_ADMIN.getRoleString())
                    .webSite("www.testwebsite.com").build();
    @EqualsAndHashCode.Exclude
    private Integer id;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Builder.Default
    private String cellphone;
    private String email;
    private String firstName;
    private String secondName;
    private String role;
    private String webSite;
    @EqualsAndHashCode.Exclude
    private Boolean confirmed;
}
