package com.epam.models;

import com.epam.models.submodels.AddressModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder(toBuilder = true)
public class ProfileModel extends BaseModel {
    public static final ProfileModel CORRECT_TEST_PROFILE_MODEL =
            ProfileModel.builder()
                    .email(UserModel.TEST_USER.getEmail())
                    .firstName("FirstName")
                    .secondName("SecondName")
                    .dateOfBirthday("2001-01-01")
                    .gender("MALE")
                    .address(AddressModel.CORRECT_TEST_ADDRESS_MODEL).build();
    private AddressModel address;
    private String dateOfBirthday;
    private String email;
    @EqualsAndHashCode.Exclude
    private String fileName;
    private String firstName;
    private String gender;
    private String secondName;
    @EqualsAndHashCode.Exclude
    private String verification;
    private String role;
}
