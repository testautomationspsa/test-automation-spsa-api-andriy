package com.epam.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;

@Data
@Builder(toBuilder = true)
public class TrainerInfoModel extends BaseModel {
    public static final TrainerInfoModel CORRECT_TRAINER_INFO_MODEL =
            TrainerInfoModel.builder()
                    .experience(3)
                    .sportType("RUNNING")
                    .socialNetworkLink(Arrays.asList("https://facebook/trainerprofile"))
                    .phoneNumber("+380631111111").build();
    @EqualsAndHashCode.Exclude
    private Integer id;
    private String phoneNumber;
    private String sportType;
    private Integer experience;
    @EqualsAndHashCode.Exclude
    private List<String> socialNetworkLink;
    @EqualsAndHashCode.Exclude
    private ProfileModel person;
    @EqualsAndHashCode.Exclude
    private Boolean approved;
    @EqualsAndHashCode.Exclude
    private Boolean work;
    @EqualsAndHashCode.Exclude
    private Integer ratingTotalNumber;
    @EqualsAndHashCode.Exclude
    private Double ratingTotalEstimation;
}
