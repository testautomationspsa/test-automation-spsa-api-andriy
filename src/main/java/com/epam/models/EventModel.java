package com.epam.models;

import com.epam.utils.Constants;
import com.epam.utils.enums.Frequency;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@Builder(toBuilder = true)
public class EventModel extends BaseModel {
    public static final EventModel CORRECT_TEST_EVENT_MODEL =
            EventModel.builder()
                    .attendeesId(Arrays.asList(Constants.TEST_TRAINER_PERSON_ID))
                    .beginTime("2001-01-21 13:00:00:000")
                    .endTime("2001-01-21 14:00:00:000")
                    .description("Test Description")
                    .frequency(Frequency.NOT_REPEAT.toString())
                    .title("Test Title").
                    build();
    @EqualsAndHashCode.Exclude
    private Integer id;
    @EqualsAndHashCode.Exclude
    private List<Integer> attendeesId;
    private String beginTime;
    private String endTime;
    private String description;
    private String frequency;
    private String title;

    public boolean equalsToEventResponseModel(EventResponseModel eventResponseModel) {
        return /*beginTime.equals(eventResponseModel.getBeginTime()) &&
                endTime.equals(eventResponseModel.getEndTime()) &&*/
                description.equals(eventResponseModel.getDescription()) &&
                frequency.equals(eventResponseModel.getFrequency()) &&
                title.equals(eventResponseModel.getTitle());
    }
}
