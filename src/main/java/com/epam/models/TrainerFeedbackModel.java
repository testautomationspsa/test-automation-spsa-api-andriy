package com.epam.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class TrainerFeedbackModel extends BaseModel{
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_MINUS_1 =
            TrainerFeedbackModel.builder().comment("Minus one star...").estimation(-1).build();
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_1 =
            TrainerFeedbackModel.builder().comment("One star...").estimation(1).build();
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_2 =
            TrainerFeedbackModel.builder().comment("Two stars...").estimation(2).build();
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_3 =
            TrainerFeedbackModel.builder().comment("Three stars...").estimation(3).build();
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_4 =
            TrainerFeedbackModel.builder().comment("Four stars...").estimation(4).build();
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_5 =
            TrainerFeedbackModel.builder().comment("Five stars...").estimation(5).build();
    public static final TrainerFeedbackModel CORRECT_TRAINER_FEEDBACK_MODEL_RATE_6 =
            TrainerFeedbackModel.builder().comment("Six stars...").estimation(6).build();
    private Integer id;
    private String comment;
    private Integer estimation;
}
