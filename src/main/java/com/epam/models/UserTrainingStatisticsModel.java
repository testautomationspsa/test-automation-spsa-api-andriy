package com.epam.models;

import lombok.Data;

@Data
public class UserTrainingStatisticsModel extends BaseModel {
    private String sportType;
    private Double totalValue;
    private Double averageValue;
}
