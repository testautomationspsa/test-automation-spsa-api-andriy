package com.epam.models;

import com.google.gson.Gson;

public class BaseModel {

    public String getInJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
