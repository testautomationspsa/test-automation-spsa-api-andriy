package com.epam.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder(toBuilder = true)
public class SimplifiedProfileModel extends BaseModel {
    @EqualsAndHashCode.Exclude
    private Integer id;
    @EqualsAndHashCode.Exclude
    private String fileName;
    private String firstName;
    private String role;
    private String secondName;
}
