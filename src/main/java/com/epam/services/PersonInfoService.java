package com.epam.services;

import com.epam.models.PersonInfoModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class PersonInfoService extends Service {
    @Step("GET Person Info step ...")
    public ValidatableResponse getPersonInfo() {
        return client.cookies(cookies).get(Constants.PERSON_INFO_PATH).then().log().all();
    }

    @Step("POST Person Info step ...")
    public ValidatableResponse postPersonInfo(PersonInfoModel personInfoModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(personInfoModel.getInJSON())
                .post(Constants.PERSON_INFO_PATH).then().log().all();
    }

    @Step("DELETE Person Info step ...")
    public ValidatableResponse deletePersonInfo() {
        return client.cookies(cookies).delete(Constants.PERSON_INFO_PATH).then().log().all();
    }
}
