package com.epam.services;

import com.epam.models.FindTrainerModel;
import com.epam.models.TrainerFeedbackModel;
import com.epam.models.TrainerInfoModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class TrainerInfoService extends Service {
    private static final String RATING_URI_ADDITION = "/rating";

    @Step("GET by id {0} trainer-info step ...")
    public ValidatableResponse getByIdTrainerInfo(int id) {
        return client.cookies(cookies).get(Constants.TRAINER_INFO_PATH + "/" + id).then().log().all();
    }

    @Step("GET by trainer id {0} trainer-info rating list step ...")
    public ValidatableResponse getTrainerInfoRatingList(int id) {
        return client.cookies(cookies).get(Constants.TRAINER_INFO_PATH + "/" + id + RATING_URI_ADDITION + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("POST by trainer id {0} trainer-info rating step ...")
    public ValidatableResponse postTrainerInfoRating(int id, TrainerFeedbackModel trainerFeedbackModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(trainerFeedbackModel.getInJSON())
                .post(Constants.TRAINER_INFO_PATH + "/" + id + RATING_URI_ADDITION).then().log().all();
    }

    @Step("GET trainer-info rating by id {0} step ...")
    public ValidatableResponse getTrainerInfoRatingById(int id) {
        return client.cookies(cookies).get(Constants.TRAINER_INFO_PATH + RATING_URI_ADDITION + "/" + id).then().log().all();
    }

    @Step("PUT trainer-info rating with id {0} step ...")
    public ValidatableResponse putTrainerInfoRating(int id, TrainerFeedbackModel trainerFeedbackModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(trainerFeedbackModel.getInJSON())
                .put(Constants.TRAINER_INFO_PATH + RATING_URI_ADDITION + "/" + id).then().log().all();
    }

    @Step("DELETE trainer-info rating with id {0} step ...")
    public ValidatableResponse deleteTrainerInfoRating(int id) {
        return client.cookies(cookies).delete(Constants.TRAINER_INFO_PATH + RATING_URI_ADDITION + "/" + id).then().log().all();
    }

    @Step("POST trainer-info step ...")
    public ValidatableResponse postTrainerInfo(TrainerInfoModel trainerInfoModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(trainerInfoModel.getInJSON())
                .post(Constants.TRAINER_INFO_PATH).then().log().all();
    }

    @Step("DELETE trainer-info with id {0} step ...")
    public ValidatableResponse deleteTrainerInfo(int id) {
        return client.cookies(cookies).delete(Constants.TRAINER_INFO_PATH + "/" + id).then().log().all();
    }

    @Step("GET by trainer id {0} trainer-info rating list step ...")
    public ValidatableResponse getFindTrainer(FindTrainerModel findTrainerModel) {
        return client.cookies(cookies).get(Constants.TRAINER_INFO_PATH + "/find-trainers" + findTrainerModel.toString()
                + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }
}
