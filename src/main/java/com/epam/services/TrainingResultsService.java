package com.epam.services;

import com.epam.models.TrainingResultsModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class TrainingResultsService extends Service {
    @Step("GET All training results step ...")
    public ValidatableResponse getAllTrainingResults() {
        return client.cookies(cookies).get(Constants.TRAINING_RESULTS_PATH + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("GET was training today step ...")
    public ValidatableResponse getWasTrainingToday() {
        return client.cookies(cookies).get(Constants.TRAINING_RESULTS_PATH + "/check-today").then().log().all();
    }

    @Step("GET All previous results and trainers step ...")
    public ValidatableResponse getAllResultsAndTrainers() {
        return client.cookies(cookies).get(Constants.TRAINING_RESULTS_PREVIOUS_DATA_PATH).then().log().all();
    }

    @Step("GET training results by id {0} step ...")
    public ValidatableResponse getTrainingResultsById(int id) {
        return client.cookies(cookies).get(Constants.TRAINING_RESULTS_PATH + "/" + id).then().log().all();
    }

    @Step("POST training results {0} step ...")
    public ValidatableResponse postTrainingResults(TrainingResultsModel trainingResultsModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(trainingResultsModel.getInJSON()).post(Constants.TRAINING_RESULTS_PATH).then().log().all();
    }

    @Step("PUT training results {0} step ...")
    public ValidatableResponse putTrainingResults(int id, TrainingResultsModel trainingResultsModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(trainingResultsModel.getInJSON()).put(Constants.TRAINING_RESULTS_PATH + "/" + id).then().log().all();
    }

    @Step("DELETE training results by id {0} step ...")
    public ValidatableResponse deleteTrainingResultsById(int id) {
        return client.cookies(cookies).delete(Constants.TRAINING_RESULTS_PATH + "/" + id).then().log().all();
    }
}
