package com.epam.services;

import com.epam.models.PostModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class PostService extends Service {
    @Step("GET by tag '{0}' post step ...")
    public ValidatableResponse getPostsByTag(FindByTagModel findByTagModel) {
        return client.cookies(cookies).get(Constants.POSTS_FIND_BY_TAG_PATH + findByTagModel.toString()).then().log().all();
    }

    @Step("GET by id {0} post step ...")
    public ValidatableResponse getPostById(int id) {
        return client.cookies(cookies).get(Constants.POSTS_PATH + "/" + id).then().log().all();
    }

    @Step("GET All posts step ...")
    public ValidatableResponse getAllPosts() {
        return client.cookies(cookies).get(Constants.POSTS_PATH + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES.toString()).then().log().all();
    }

    @Step("POST post step ...")
    public ValidatableResponse postPost(PostModel postModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(postModel.getInJSON())
                .post(Constants.POSTS_PATH).then().log().all();
    }

    @Step("PUT post step with id {1}...")
    public ValidatableResponse putPost(PostModel postModel, Integer id) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(postModel.getInJSON())
                .put(Constants.POSTS_PATH + "/" + id).then().log().all();
    }

    @Step("DELETE by id {0} post step ...")
    public ValidatableResponse deleteByIdPost(int id) {
        return client.cookies(cookies).delete(Constants.POSTS_PATH + "/" + id).then().log().all();
    }

    public class FindByTagModel {
        private Integer page;
        private Integer size;
        private String tag;

        public FindByTagModel setPage(Integer page) {
            this.page = page;
            return this;
        }

        public FindByTagModel setSize(Integer size) {
            this.size = size;
            return this;
        }

        public FindByTagModel setTag(String tag) {
            this.tag = tag;
            return this;
        }

        @Override
        public String toString() {
            return "?page=" + page +
                    "&size=" + size +
                    "&tag=" + tag;
        }
    }
}
