package com.epam.services;

import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

public class CurrentPersonStatisticsService extends Service {
    private static String DAY = "/day";
    private static String WEEK = "/days/seven";
    private static String TWO_WEEKS = "/days/fourteen";
    private static String MONTH_CURRENT = "/month/current";
    private static String MONTH_PREVIOUS = "/month/previous";
    private static String YEAR = "/year";

    @Step("GET current person statistics for day step ...")
    public ValidatableResponse getCurrentPersonStatisticsForDayTest(String sportType) {
        return client.cookies(cookies).get(Constants.CURRENT_PERSON_STATISTICS_PATH + DAY + "/" +
                sportType).then().log().all();
    }

    @Step("GET current person statistics for week step ...")
    public ValidatableResponse getCurrentPersonStatisticsForWeekTest(String sportType) {
        return client.cookies(cookies).get(Constants.CURRENT_PERSON_STATISTICS_PATH + WEEK + "/" +
                sportType).then().log().all();
    }

    @Step("GET current person statistics for two weeks step ...")
    public ValidatableResponse getCurrentPersonStatisticsForTwoWeeksTest(String sportType) {
        return client.cookies(cookies).get(Constants.CURRENT_PERSON_STATISTICS_PATH + TWO_WEEKS + "/" +
                sportType).then().log().all();
    }

    @Step("GET current person statistics for current month step ...")
    public ValidatableResponse getCurrentPersonStatisticsForMonthCurrentTest(String sportType) {
        return client.cookies(cookies).get(Constants.CURRENT_PERSON_STATISTICS_PATH + MONTH_CURRENT + "/" +
                sportType).then().log().all();
    }

    @Step("GET current person statistics for previous month step ...")
    public ValidatableResponse getCurrentPersonStatisticsForMonthPreviousTest(String sportType) {
        return client.cookies(cookies).get(Constants.CURRENT_PERSON_STATISTICS_PATH + MONTH_PREVIOUS + "/" +
                sportType).then().log().all();
    }

    @Step("GET current person statistics for year step ...")
    public ValidatableResponse getCurrentPersonStatisticsForYearTest(String sportType) {
        return client.cookies(cookies).get(Constants.CURRENT_PERSON_STATISTICS_PATH + YEAR + "/" +
                sportType).then().log().all();
    }
}
