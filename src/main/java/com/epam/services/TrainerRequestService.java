package com.epam.services;

import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

public class TrainerRequestService extends Service {
    @Step("POST trainer Request for Work step ...")
    public ValidatableResponse postTrainerRequestForWork() {
        return client.cookies(cookies).post(Constants.TRAINER_REQUEST_PATH + "/for-work").then().log().all();
    }

    @Step("POST trainer Request for Firing step ...")
    public ValidatableResponse postTrainerRequestForFiring() {
        return client.cookies(cookies).post(Constants.TRAINER_REQUEST_PATH + "/for-firing").then().log().all();
    }

    @Step("POST approve trainer request with id {1] by venue with id {0} step ...")
    public ValidatableResponse postApproveTrainerRequest(int venueId, int requestId) {
        return client.cookies(cookies).post(Constants.TRAINER_REQUEST_PATH + "/approve/" + requestId +
                "/venue/" + venueId).then().log().all();
    }

    @Step("GET All trainers requests for venue with id {0} step ...")
    public ValidatableResponse getAllTrainersRequests(int venueId) {
        return client.cookies(cookies).get(Constants.TRAINER_REQUEST_PATH + "/all/check/venue/" +
                venueId + "?" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("GET Trainers Who want to Work in venue with id {0} step ...")
    public ValidatableResponse getTrainersWhoWontToWorkInVenue(int venueId) {
        return client.cookies(cookies).get(Constants.TRAINER_REQUEST_PATH + "/check/venue/" + venueId + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("GET Trainer own requests step ...")
    public ValidatableResponse getTrainerOwnRequests() {
        return client.cookies(cookies).get(Constants.TRAINER_REQUEST_PATH + "/own" +
                "?" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("DELETE trainer Request with id {0} step ...")
    public ValidatableResponse deleteTrainerRequestById(int id) {
        return client.cookies(cookies).delete(Constants.TRAINER_REQUEST_PATH + "/request/" + id).then().log().all();
    }

    @Step("POST trainer Request for venue with id {0} step ...")
    public ValidatableResponse postTrainerRequestForVenue(int venueId) {
        return client.cookies(cookies).post(Constants.TRAINER_REQUEST_PATH + "/for-work/venue/" + venueId).then().log().all();
    }
}
