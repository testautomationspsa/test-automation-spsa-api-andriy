package com.epam.services;

import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

public class TotalStatisticsService extends Service {
    @Step("GET total training statistics step ...")
    public ValidatableResponse getTotalTrainingStatistics() {
        return client.cookies(cookies).get(Constants.TOTAL_TRAINING_STATISTICS_PATH).then().log().all();
    }
}
