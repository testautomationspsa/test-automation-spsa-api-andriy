package com.epam.services;

import com.epam.models.UserModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.given;

public class AuthenticationService extends Service {
    @Step("Post auth for user {0} step")
    public ValidatableResponse postAuthLogin(UserModel user) {
        client = given()
                .log()
                .all()
                .baseUri(Constants.BASE_PATH);
        return client.contentType(ContentType.JSON)
                .body(user.formCredentialsJSON())
                .post(Constants.LOGIN_PATH).then().log().all();
    }
}
