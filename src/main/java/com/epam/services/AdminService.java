package com.epam.services;

import com.epam.models.EntitlementFormModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class AdminService extends Service {
    @Step("GET All confirmed forms step ...")
    public ValidatableResponse getAllConfirmedForms() {
        return client.cookies(cookies).get(Constants.ADMIN_GET_CONFIRMED_FORMS_PATH + "&" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("GET All unread forms step ...")
    public ValidatableResponse getAllUnreadForms() {
        return client.cookies(cookies).get(Constants.ADMIN_UNREAD_PATH + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("POST Create Entitlement form step ...")
    public ValidatableResponse postCreateEntitlementForm(EntitlementFormModel entitlementFormModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(entitlementFormModel.getInJSON())
                .post(Constants.ADMIN_PATH).then().log().all();
    }

    @Step("DELETE by id {0} Entitlement form step ...")
    public ValidatableResponse deleteByIdEntitlementForm(int id) {
        return client.cookies(cookies).delete(Constants.ADMIN_PATH + "/" + id).then().log().all();
    }

    @Step("POST Confirm Entitlement form step ...")
    public ValidatableResponse postConfirmEntitlementForm(int id) {
        return client.cookies(cookies).post(Constants.ADMIN_PATH + "/" + id + "/confirm").then().log().all();
    }

    @Step("POST Reject Entitlement form step ...")
    public ValidatableResponse postRejectEntitlementForm(int id) {
        return client.cookies(cookies).post(Constants.ADMIN_PATH + "/" + id + "/reject").then().log().all();
    }

    @Step("PUT set user with id {0} role {1} step ...")
    public ValidatableResponse putSetUserRole(int userId) {
        return client.cookies(cookies).put("/admin/persons/" + userId + "/toUser").then().log().all();
    }
}
