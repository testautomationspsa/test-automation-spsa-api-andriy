package com.epam.services;

import com.epam.listeners.CustomTestNGListener;
import com.epam.models.UserModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.http.Cookies;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Listeners;

import static io.restassured.RestAssured.given;

@Listeners(CustomTestNGListener.class)
public class Service {
    protected Cookies cookies;
    protected RequestSpecification client;

    @Step("Initializing user-client step.")
    public void initClient(UserModel user) {
        client = given()
                .log()
                .all()
                .baseUri(Constants.BASE_PATH);
        cookies = client.contentType(ContentType.JSON)
                .body(user.formCredentialsJSON())
                .post(Constants.LOGIN_PATH)
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .response()
                .getDetailedCookies();
    }

    @Step("Log Out step")
    public ValidatableResponse logOut() {
        return client.cookies(cookies).post("/logout").then().log().all();
    }
}
