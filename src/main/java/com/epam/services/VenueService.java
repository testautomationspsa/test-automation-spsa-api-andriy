package com.epam.services;

import com.epam.models.VenueModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class VenueService extends Service {
    @Step("GET All user venues step ...")
    public ValidatableResponse getAllUserVenues() {
        return client.cookies(cookies).get(Constants.VENUE_PATH + "?" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("GET venue by id {0} step ...")
    public ValidatableResponse getVenueById(int id) {
        return client.cookies(cookies).get(Constants.VENUE_PATH + "/" + id).then().log().all();
    }

    @Step("GET All admin venues step ...")
    public ValidatableResponse getAllAdminVenues() {
        return client.cookies(cookies).get(Constants.VENUE_ADMIN_PATH + "?" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("POST venue step ...")
    public ValidatableResponse postVenue(VenueModel venueModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(venueModel.getInJSON())
                .post(Constants.VENUE_PATH).then().log().all();
    }

    @Step("PUT venue step ...")
    public ValidatableResponse putVenue(VenueModel testVenueModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(testVenueModel.getInJSON()).put(Constants.VENUE_PATH).then().log().all();
    }

    @Step("DELETE by id {0} venue step ...")
    public ValidatableResponse deleteByIdVenue(int id) {
        return client.cookies(cookies).delete(Constants.VENUE_PATH + "/" + id).then().log().all();
    }

    @Step("GET find venues by example step ...")
    public ValidatableResponse findVenuesByExample(VenueModel venueModel) {
        return client.cookies(cookies).get(Constants.VENUE_FIND_PATH + venueModel.toString() + "&" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("GET All trainers who work in venue step ...")
    public ValidatableResponse getAllTrainersWhoWorkInVenue(int id) {
        return client.cookies(cookies).get(Constants.VENUE_PATH + "/" + id + Constants.VENUE_GET_ALL_TRAINERS_WHO_WORK_IN_PATH +
                "?" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("DELETE trainer with TrainerInfId {1} from venue with id {0} step ...")
    public ValidatableResponse deleteTrainerFromVenue(int venueId, int trainerId) {
        return client.cookies(cookies).delete(Constants.VENUE_PATH + "/" + venueId + "/trainer/" + trainerId).then().log().all();
    }
}
