package com.epam.services;

import com.epam.models.submodels.AddressModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class AddressService extends Service {
    @Step("GET address step ...")
    public ValidatableResponse getAddress() {
        return client.cookies(cookies).get(Constants.ADDRESS_PATH).then().log().all();
    }

    @Step("POST address step ...")
    public ValidatableResponse postAddress(AddressModel addressModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(addressModel.getInJSON())
                .post(Constants.ADDRESS_PATH).then().log().all();
    }

    @Step("PUT address step ...")
    public ValidatableResponse putAddress() {
        return client.cookies(cookies).put(Constants.ADDRESS_PATH).then().log().all();
    }

    @Step("DELETE address step ...")
    public ValidatableResponse deleteAddress() {
        return client.cookies(cookies).delete(Constants.ADDRESS_PATH).then().log().all();
    }
}
