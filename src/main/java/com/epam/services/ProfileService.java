package com.epam.services;

import com.epam.models.ProfileModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class ProfileService extends Service {
    @Step("GET profile step ...")
    public ValidatableResponse getProfile() {
        return client.cookies(cookies).get(Constants.PROFILE_PATH).then().log().all();
    }

    @Step("POST profile step ...")
    public ValidatableResponse postProfile(ProfileModel profileModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(profileModel.getInJSON())
                .post(Constants.PROFILE_PATH).then().log().all().using();
    }
}
