package com.epam.services;

import com.epam.models.RequestModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class RequestService extends Service {
    @Step("GET by id {0} request step ...")
    public ValidatableResponse getByIdRequest(int id) {
        return client.cookies(cookies).get(Constants.REQUESTS_PATH + "/" + id).then().log().all();
    }

    @Step("GET All requests step ...")
    public ValidatableResponse getAllRequest() {
        return client.cookies(cookies).get(Constants.REQUESTS_PATH + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES).then().log().all();
    }

    @Step("POST request step ...")
    public ValidatableResponse postRequest(RequestModel requestModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(requestModel.getInJSON())
                .post(Constants.REQUESTS_PATH).then().log().all();
    }

    @Step("PUT request step ...")
    public ValidatableResponse putRequest(int requestId, RequestModel requestModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(requestModel.getInJSON())
                .put(Constants.REQUESTS_PATH + "/" + requestId).then().log().all();
    }

    @Step("GET possible partners step ...")
    public ValidatableResponse getPossiblePartnersRequest() {
        return client.cookies(cookies).get(Constants.REQUESTS_POSSIBLE_PARTNERS_PATH + "?" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES.toString()).then().log().all();
    }

    @Step("DELETE by id {0} request step ...")
    public ValidatableResponse deleteByIdRequest(int id) {
        return client.cookies(cookies).delete(Constants.REQUESTS_PATH + "/" + id).then().log().all();
    }

    @Step("DELETE ALL requests step ...")
    public ValidatableResponse deleteAllRequest() {
        return client.cookies(cookies).delete(Constants.REQUESTS_PATH).then().log().all();
    }

    @Step("GET find partners by request step ...")
    public ValidatableResponse findPartnersByRequest(RequestModel requestModel) {
        return client.cookies(cookies).log().all().get(Constants.FIND_PARTNERS_PATH +
                requestModel.toString() + "&" +
                GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES.toString()).then().log().all();
    }
}
