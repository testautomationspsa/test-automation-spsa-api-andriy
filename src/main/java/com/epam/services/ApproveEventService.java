package com.epam.services;

import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;

public class ApproveEventService extends Service {
    @Step("GET by id {0} approved event step ...")
    public ValidatableResponse getByIdApprovedEvent(int id) {
        return client.cookies(cookies).get(Constants.APPROVE_EVENT_PATH + "/" + id).then().log().all();
    }

    @Step("GET all user events step ...")
    public ValidatableResponse getAllUserEvents() {
        return client.cookies(cookies).get(Constants.APPROVE_EVENT_PATH + "/").then().log().all();
    }

    @Step("PUT approve event with id {0} step ...")
    public ValidatableResponse putApproveEvent(int eventId, boolean confirmed) {
        return client.cookies(cookies).put(Constants.APPROVE_EVENT_PATH + "/" + eventId +
                "?confirmed=" + confirmed).then().log().all();
    }

    @Step("DELETE by id {0} approved event step ...")
    public ValidatableResponse deleteByIdApprovedEvent(int id) {
        return client.cookies(cookies).delete(Constants.APPROVE_EVENT_PATH + "/" + id).then().log().all();
    }
}
