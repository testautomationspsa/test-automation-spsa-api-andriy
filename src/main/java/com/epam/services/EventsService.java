package com.epam.services;

import com.epam.models.EventModel;
import com.epam.models.submodels.GetPageSizeModel;
import com.epam.utils.Constants;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

public class EventsService extends Service {
    @Step("GET by id {0} event step ...")
    public ValidatableResponse getByIdEvent(int id) {
        return client.cookies(cookies).get(Constants.EVENTS_PATH + "/" + id).then().log().all();
    }

    @Step("GET all user's events by month {0} and year {1} event step ...")
    public ValidatableResponse getAllUserEventsByMonth(String month, String year) {
        return client.cookies(cookies).get(Constants.EVENTS_PATH + "?month=" + month + "&year=" + year +
                "&" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES)
                .then().log().all();
    }

    @Step("POST event {0} step ...")
    public ValidatableResponse postEvent(EventModel eventModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(eventModel.getInJSON())
                .post(Constants.EVENTS_PATH).then().log().all();
    }

    @Step("PUT event with id {0} to {1} step ...")
    public ValidatableResponse putEvent(int eventId, EventModel eventModel) {
        return client.cookies(cookies).contentType(ContentType.JSON)
                .body(eventModel.getInJSON())
                .put(Constants.EVENTS_PATH + "/" + eventId).then().log().all();
    }

    @Step("DELETE by id {0} event step ...")
    public ValidatableResponse deleteByIdEvent(int id) {
        return client.cookies(cookies).delete(Constants.EVENTS_PATH + "/" + id).then().log().all();
    }

    @Step("GET all persons for event step ...")
    public ValidatableResponse getFindAllPersonsForEventEvents() {
        return client.cookies(cookies).get(Constants.EVENTS_PATH + "/persons" +
                "?" + GetPageSizeModel.CORRECT_PAGE_SIZE_VALUES)
                .then().log().all();
    }
}
