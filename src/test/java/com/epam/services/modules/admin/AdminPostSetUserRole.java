package com.epam.services.modules.admin;

import com.epam.fortest.BaseTest;
import com.epam.models.ProfileModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.AdminService;
import com.epam.services.ProfileService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import com.epam.utils.enums.Role;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AdminPostSetUserRole extends BaseTest {
    private AdminService adminService;
    private ProfileService profileService;

    @BeforeMethod
    void init() {
        adminService = new AdminService();
        profileService = new ProfileService();
        adminService.initClient(UserModel.TEST_SUPER_ADMIN);
    }

    @Test()
    void postSetUserRole() {
        new CustomResponse(adminService.putSetUserRole(Constants.TEST_USER_PERSON_ID))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        adminService.logOut();
        profileService.initClient(UserModel.TEST_USER);
        ProfileModel testUser = new CustomResponse(profileService.getProfile()).getResponseProfileModel();
        profileService.logOut();
        Assert.assertEquals(testUser.getRole(), Role.USER.getRoleString(),
                "Role was set incorrectly!");
    }
}
