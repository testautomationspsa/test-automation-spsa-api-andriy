package com.epam.services.modules.admin;

import com.epam.fortest.BaseTest;
import com.epam.models.EntitlementFormModel;
import com.epam.models.ProfileModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.AdminService;
import com.epam.services.ProfileService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AdminPostConfirm extends BaseTest {
    private Integer createdFormId = 0;
    private AdminService adminService;
    private ProfileService profileService;

    @BeforeMethod
    void init() {
        adminService = new AdminService();
        profileService = new ProfileService();
        adminService.initClient(UserModel.TEST_USER);
        createdFormId = new CustomResponse(adminService.postCreateEntitlementForm(
                EntitlementFormModel.CORRECT_TEST_ENTITLEMENT_MODEL.toBuilder()
                        .email(UserModel.TEST_USER.getEmail()).build()))
                .getResponseEntitlementModel().getId();
        adminService.logOut();
    }

    @Test()
    void postConfirmForm() {
        adminService.initClient(UserModel.TEST_SUPER_ADMIN);
        new CustomResponse(adminService.postConfirmEntitlementForm(createdFormId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseEntitlementModel();
        new CustomResponse(adminService.postConfirmEntitlementForm(Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        adminService.logOut();
        profileService.initClient(UserModel.TEST_USER);
        ProfileModel testUser = new CustomResponse(profileService.getProfile()).getResponseProfileModel();
        profileService.logOut();
        Assert.assertEquals(testUser.getRole(), EntitlementFormModel.CORRECT_TEST_ENTITLEMENT_MODEL.getRole(),
                "Role was set incorrectly!");
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        adminService.putSetUserRole(Constants.TEST_USER_PERSON_ID);
        adminService.logOut();
    }
}
