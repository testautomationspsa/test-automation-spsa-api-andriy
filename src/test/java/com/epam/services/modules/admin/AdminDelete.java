package com.epam.services.modules.admin;

import com.epam.fortest.BaseTest;
import com.epam.models.EntitlementFormModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.AdminService;
import com.epam.utils.StringRandomizer;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AdminDelete extends BaseTest {
    private Integer createdFormId = 0;
    private AdminService adminService = new AdminService();

    @BeforeMethod
    void init() {
        adminService = new AdminService();
        adminService.initClient(UserModel.TEST_USER);
    }

    @Test()
    void deleteFormTest() {
        createdFormId = new CustomResponse(adminService.postCreateEntitlementForm(
                EntitlementFormModel.CORRECT_TEST_ENTITLEMENT_MODEL.toBuilder()
                        .email(StringRandomizer.getRandomMail(5)).build()))
                .getResponseEntitlementModel().getId();
        adminService.initClient(UserModel.TEST_SUPER_ADMIN);
        new CustomResponse(adminService.deleteByIdEntitlementForm(createdFormId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(adminService.deleteByIdEntitlementForm(createdFormId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
