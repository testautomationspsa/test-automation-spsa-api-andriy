package com.epam.services.modules.admin;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.AdminService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AdminGetAllConfirmed extends BaseTest {
    private AdminService adminService = new AdminService();

    @BeforeMethod
    void init() {
        adminService = new AdminService();
        adminService.initClient(UserModel.TEST_SUPER_ADMIN);
    }

    @Test()
    void getAllConfirmedForms() {
        new CustomResponse(adminService.getAllConfirmedForms())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
