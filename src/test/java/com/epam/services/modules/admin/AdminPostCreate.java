package com.epam.services.modules.admin;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.EntitlementFormModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.AdminService;
import com.epam.utils.StringRandomizer;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Objects;

public class AdminPostCreate extends BaseTest {
    private Integer createdFormId = 0;
    private AdminService adminService = new AdminService();

    @BeforeMethod
    void init() {
        adminService = new AdminService();
        adminService.initClient(UserModel.TEST_USER);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "formsData")
    void postCreateForm(String cellphone, String role, String website, String responseStatus) {
        EntitlementFormModel toBeCreatedEntitlementForm = EntitlementFormModel.CORRECT_TEST_ENTITLEMENT_MODEL
                .toBuilder()
                .email(StringRandomizer.getRandomMail(5))
                .cellphone(cellphone)
                .role(role)
                .webSite(website).build();
        EntitlementFormModel responseEntitlementForm = new CustomResponse(adminService.postCreateEntitlementForm(
                toBeCreatedEntitlementForm))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus)).getResponseEntitlementModel();
        if (Objects.nonNull(responseEntitlementForm)) {
            createdFormId = responseEntitlementForm.getId();
            Assert.assertEquals(toBeCreatedEntitlementForm, responseEntitlementForm,
                    "Entitlement form created incorrectly!");
        }
    }

    @AfterMethod
    void clean() {
        adminService.initClient(UserModel.TEST_SUPER_ADMIN);
        adminService.deleteByIdEntitlementForm(createdFormId);
    }
}
