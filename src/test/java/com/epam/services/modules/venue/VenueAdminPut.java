package com.epam.services.modules.venue;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.VenueModel;
import com.epam.models.response.CustomResponse;
import com.epam.models.submodels.AddressModel;
import com.epam.services.VenueService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class VenueAdminPut extends BaseTest {
    private VenueService venueService;
    private Integer createdVenueId = 0;

    @BeforeMethod
    void init() {
        venueService = new VenueService();
        venueService.initClient(UserModel.TEST_VENUE_ADMIN);
        createdVenueId = new CustomResponse(venueService.postVenue(VenueModel.CORRECT_TEST_VENUE_MODEL))
                .getResponseVenueModel().getId();
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "venueData")
    void putVenueTest(String distance, String description, String inOut, String name, String sportTypes, String responseStatus) {
        VenueModel testVenueModel = formVenueModel(distance, description, inOut, name, sportTypes);
        VenueModel responseModel = new CustomResponse(venueService.putVenue(testVenueModel))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus))
                .getResponseVenueModel();
        if (ResponseStatus.valueOf(responseStatus) == ResponseStatus.SUCCESS) {
            Assert.assertEquals(responseModel, testVenueModel);
        }
    }

    @Test
    void putVenueIncorrectOpenClosedTimeTest() {
        VenueModel testVenueModel = VenueModel.CORRECT_TEST_VENUE_MODEL.toBuilder()
                .openTime("").build();
        new CustomResponse(venueService.putVenue(testVenueModel))
                .verifyStatusCode(ResponseStatus.VALIDATION_MISTAKE)
                .verifyStatus(ResponseStatus.VALIDATION_MISTAKE);
    }

    @Test
    void putVenueIncorrectSportTypeTest() {
        VenueModel testVenueModel = VenueModel.CORRECT_TEST_VENUE_MODEL.toBuilder()
                .openTime("").build();
        new CustomResponse(venueService.putVenue(testVenueModel))
                .verifyStatusCode(ResponseStatus.VALIDATION_MISTAKE)
                .verifyStatus(ResponseStatus.VALIDATION_MISTAKE);
        testVenueModel = VenueModel.CORRECT_TEST_VENUE_MODEL.toBuilder()
                .sportType(new ArrayList<>()).build();
        new CustomResponse(venueService.putVenue(testVenueModel))
                .verifyStatusCode(ResponseStatus.VALIDATION_MISTAKE)
                .verifyStatus(ResponseStatus.VALIDATION_MISTAKE);
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        venueService.deleteByIdVenue(createdVenueId);
    }

    private VenueModel formVenueModel(String distance, String description, String inOut, String name, String sportTypes) {
        return VenueModel.CORRECT_TEST_VENUE_MODEL.toBuilder()
                .distance(distance.equals("") ? null : Integer.valueOf(distance))
                .description(description.equals("") ? null : description)
                .inOut(inOut.equals("") ? null : inOut)
                .name(name.equals("") ? null : name)
                .sportType(sportTypes.equals("") ? Arrays.asList() : Arrays.asList(sportTypes))
                .address(AddressModel.CORRECT_TEST_ADDRESS_MODEL)
                .id(createdVenueId).build();
    }
}
