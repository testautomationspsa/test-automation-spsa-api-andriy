package com.epam.services.modules.venue;

import com.epam.fortest.BaseTest;
import com.epam.models.response.CustomResponse;
import com.epam.models.UserModel;
import com.epam.services.VenueService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VenueUserGetAll extends BaseTest {
    private VenueService venueService;

    @BeforeMethod
    void init() {
        venueService = new VenueService();
        venueService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getAllUserVenuesTest() {
        new CustomResponse(venueService.getAllUserVenues())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
