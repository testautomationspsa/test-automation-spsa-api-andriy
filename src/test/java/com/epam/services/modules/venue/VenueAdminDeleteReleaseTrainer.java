package com.epam.services.modules.venue;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainerRequestModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.services.TrainerRequestService;
import com.epam.services.VenueService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class VenueAdminDeleteReleaseTrainer extends BaseTest {
    private TrainerRequestService trainerRequestService;
    private TrainerInfoService trainerInfoService;
    private VenueService venueService;
    private Integer createdTrainerRequestId = 0;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerRequestService = new TrainerRequestService();
        venueService = new VenueService();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForWork())
                .getResponseTrainerRequestModel().getId();
        trainerRequestService.logOut();
        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
        trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID, createdTrainerRequestId);
        trainerRequestService.logOut();
    }

    @Test
    void deleteTrainerFromVenueTest() {
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        List<TrainerRequestModel> trainerRequests = new CustomResponse(trainerRequestService
                .getTrainerOwnRequests())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS).getLongResponseTrainerRequestModelList();
        Assert.assertTrue(trainerRequests.isEmpty());
        trainerRequestService.logOut();

        Boolean workStatus = getTrainerWorkStatus();
        Assert.assertTrue(workStatus, "Trainer work status is not true after approving request.");
        trainerInfoService.logOut();

        /*trainerRequestService.initClient(UserModel.TEST_TRAINER);
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForFiring())
                .getResponseTrainerRequestModel().getId();
        trainerRequestService.logOut();

        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
        trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID, createdTrainerRequestId);
        trainerRequestService.logOut();*/

        venueService.initClient(UserModel.TEST_VENUE_ADMIN);
        new CustomResponse(venueService.deleteTrainerFromVenue(Constants.TEST_VENUE_ID, Constants.TEST_TRAINER_INFO_ID))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(venueService.deleteTrainerFromVenue(Integer.MAX_VALUE, Constants.TEST_TRAINER_INFO_ID))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        new CustomResponse(venueService.deleteTrainerFromVenue(Constants.TEST_VENUE_ID, Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        venueService.logOut();

        workStatus = getTrainerWorkStatus();
        trainerInfoService.logOut();
        Assert.assertFalse(workStatus, "Trainer work status is not false after firing.");
    }

    private Boolean getTrainerWorkStatus() {
        trainerInfoService.initClient(UserModel.TEST_TRAINER);
        return new CustomResponse(trainerInfoService
                .getByIdTrainerInfo(Constants.TEST_TRAINER_INFO_ID))
                .getResponseTrainerInfoModel().getWork();
    }
}
