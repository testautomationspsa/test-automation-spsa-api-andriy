package com.epam.services.modules.venue;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.VenueModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.VenueService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VenueAdminDelete extends BaseTest {
    private VenueService venueService;
    private Integer createdVenueId = 0;

    @BeforeMethod
    void init() {
        venueService = new VenueService();
        venueService.initClient(UserModel.TEST_VENUE_ADMIN);
        createdVenueId = new CustomResponse(venueService.postVenue(VenueModel.CORRECT_TEST_VENUE_MODEL))
                .getResponseVenueModel().getId();
    }

    @Test
    void deleteVenueTest() {
        new CustomResponse(venueService.deleteByIdVenue(createdVenueId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(venueService.deleteByIdVenue(createdVenueId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
