package com.epam.services.modules.venue;

import com.epam.fortest.BaseTest;
import com.epam.models.response.CustomResponse;
import com.epam.models.UserModel;
import com.epam.services.VenueService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VenueAdminGetAll extends BaseTest {
    private VenueService venueService;

    @BeforeMethod
    void init() {
        venueService = new VenueService();
        venueService.initClient(UserModel.TEST_VENUE_ADMIN);
    }

    @Test
    void getAllAdminVenuesTest() {
        new CustomResponse(venueService.getAllAdminVenues())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
