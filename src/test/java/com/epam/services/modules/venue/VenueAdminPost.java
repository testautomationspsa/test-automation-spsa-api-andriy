package com.epam.services.modules.venue;

import com.epam.fortest.BaseTest;
import com.epam.models.response.CustomResponse;
import com.epam.models.UserModel;
import com.epam.models.VenueModel;
import com.epam.services.VenueService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VenueAdminPost extends BaseTest {
    private VenueService venueService;
    private Integer createdVenueId=0;

    @BeforeMethod
    void init() {
        venueService = new VenueService();
        venueService.initClient(UserModel.TEST_VENUE_ADMIN);
    }

    @Test
    void postVenueTest(){
        createdVenueId=new CustomResponse(venueService.postVenue(VenueModel.CORRECT_TEST_VENUE_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseVenueModel().getId();
    }

    @AfterMethod(alwaysRun = true)
    void clean(){
        venueService.deleteByIdVenue(createdVenueId);
    }
}
