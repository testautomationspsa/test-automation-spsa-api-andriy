package com.epam.services.modules.authentication;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.AuthenticationService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.Test;

public class AuthPostLogin extends BaseTest {
    AuthenticationService authenticationService;

    @Test(dataProviderClass = DataProviders.class, dataProvider = "authData")
    void authPostLoginTest(String email, String password, String responseStatus) {
        authenticationService = new AuthenticationService();
        UserModel user = new UserModel(email, password, "");
        new CustomResponse(authenticationService.postAuthLogin(user))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus));
    }
}
