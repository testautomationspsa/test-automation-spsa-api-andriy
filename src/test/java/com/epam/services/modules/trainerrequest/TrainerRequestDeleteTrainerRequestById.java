package com.epam.services.modules.trainerrequest;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerRequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerRequestDeleteTrainerRequestById extends BaseTest {
    private TrainerRequestService trainerRequestService;
    private Integer createdTrainerRequestId = 0;

    @BeforeMethod
    void init() {
        trainerRequestService = new TrainerRequestService();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForWork())
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainerRequestModel()
                .getId();
    }

    @Test
    void deleteTrainerRequestByIdTest() {
        new CustomResponse(trainerRequestService.deleteTrainerRequestById(createdTrainerRequestId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(trainerRequestService.deleteTrainerRequestById(Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        trainerRequestService.deleteTrainerRequestById(createdTrainerRequestId);
    }
}
