package com.epam.services.modules.trainerrequest;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerRequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerRequestPostTrainerRequestForWork extends BaseTest {
    private TrainerRequestService trainerRequestService;
    private Integer createdTrainerRequestId = 0;

    @BeforeMethod
    void init() {
        trainerRequestService = new TrainerRequestService();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
    }

    @Test
    void postTrainerRequestForWorkTest() {
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForWork())
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainerRequestModel()
                .getId();
        new CustomResponse(trainerRequestService.postTrainerRequestForWork())
                .verifyStatusCode(ResponseStatus.BAD_REQUEST)
                .verifyStatus(ResponseStatus.BAD_REQUEST);
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        trainerRequestService.deleteTrainerRequestById(createdTrainerRequestId);
    }
}
