package com.epam.services.modules.trainerrequest;

import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerRequestService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerRequestPostTrainerRequestForFiring {
    private TrainerRequestService trainerRequestService;
    private Integer createdTrainerRequestId = 0;

    @BeforeMethod
    void init() {
        trainerRequestService = new TrainerRequestService();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForWork())
                .getResponseTrainerRequestModel()
                .getId();
        trainerRequestService.logOut();
        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
        new CustomResponse(trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID,
                createdTrainerRequestId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        trainerRequestService.logOut();
    }

    @Test
    void postTrainerRequestForFiringTest() {
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForFiring())
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainerRequestModel()
                .getId();
        new CustomResponse(trainerRequestService.postTrainerRequestForFiring())
                .verifyStatusCode(ResponseStatus.BAD_REQUEST)
                .verifyStatus(ResponseStatus.BAD_REQUEST);
        trainerRequestService.logOut();
        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
        new CustomResponse(trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID,
                createdTrainerRequestId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        trainerRequestService.logOut();
    }
}
