package com.epam.services.modules.trainerrequest;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerRequestService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerRequestGetAllTrainersWhoWantToWorkInVenue extends BaseTest {
    private TrainerRequestService trainerRequestService;

    @BeforeMethod
    void init() {
        trainerRequestService = new TrainerRequestService();
        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
    }

    @Test
    void getAllTrainersWhoWontToWorkInVenueTest() {
        new CustomResponse(trainerRequestService.getTrainersWhoWontToWorkInVenue(Constants.TEST_VENUE_ID))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
