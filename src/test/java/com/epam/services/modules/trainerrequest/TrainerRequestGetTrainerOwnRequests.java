package com.epam.services.modules.trainerrequest;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerRequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerRequestGetTrainerOwnRequests extends BaseTest {
    private TrainerRequestService trainerRequestService;

    @BeforeMethod
    void init() {
        trainerRequestService = new TrainerRequestService();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
    }

    @Test
    void getTrainerOwnRequestsTest() {
        new CustomResponse(trainerRequestService.getTrainerOwnRequests())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
