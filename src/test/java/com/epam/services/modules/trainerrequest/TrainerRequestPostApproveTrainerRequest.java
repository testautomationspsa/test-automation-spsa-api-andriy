package com.epam.services.modules.trainerrequest;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainerRequestModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerRequestService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class TrainerRequestPostApproveTrainerRequest extends BaseTest {
    private TrainerRequestService trainerRequestService;
    private Integer createdTrainerRequestId = 0;

    @BeforeMethod
    void init() {
        trainerRequestService = new TrainerRequestService();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
    }

    @Test
    void postApproveTrainerRequestTest() {
        createdTrainerRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForWork())
                .getResponseTrainerRequestModel()
                .getId();
        approveRequest();
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        List<TrainerRequestModel> trainerRequests = new CustomResponse(trainerRequestService
                .getTrainerOwnRequests())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS).getLongResponseTrainerRequestModelList();
        Assert.assertTrue(trainerRequests.isEmpty());
        trainerRequestService.logOut();
    }

    private void approveRequest() {
        new CustomResponse(trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID,
                createdTrainerRequestId))
                .verifyStatusCode(ResponseStatus.BAD_REQUEST)
                .verifyStatus(ResponseStatus.BAD_REQUEST);
        trainerRequestService.logOut();
        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
        new CustomResponse(trainerRequestService.postApproveTrainerRequest(Integer.MAX_VALUE, createdTrainerRequestId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        new CustomResponse(trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID, Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        new CustomResponse(trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID,
                createdTrainerRequestId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        trainerRequestService.logOut();
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        Integer createdFireRequestId;
        trainerRequestService.initClient(UserModel.TEST_TRAINER);
        createdFireRequestId = new CustomResponse(trainerRequestService.postTrainerRequestForFiring())
                .getResponseTrainerRequestModel().getId();
        trainerRequestService.logOut();
        trainerRequestService.initClient(UserModel.TEST_VENUE_ADMIN);
        trainerRequestService.postApproveTrainerRequest(Constants.TEST_VENUE_ID, createdFireRequestId);
        trainerRequestService.logOut();
    }
}
