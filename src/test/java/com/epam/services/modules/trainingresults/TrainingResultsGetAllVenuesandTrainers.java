package com.epam.services.modules.trainingresults;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainingResultsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainingResultsGetAllVenuesandTrainers extends BaseTest {
    private TrainingResultsService trainingResultsService;

    @BeforeMethod
    void init() {
        trainingResultsService = new TrainingResultsService();
        trainingResultsService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getAllTrainingResultsTest() {
        new CustomResponse(trainingResultsService.getAllResultsAndTrainers())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseAllVenuesandTrainersModel();
    }
}
