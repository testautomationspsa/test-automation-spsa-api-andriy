package com.epam.services.modules.trainingresults;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainingResultsModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainingResultsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainingResultsPost extends BaseTest {
    private TrainingResultsService trainingResultsService;
    private Integer createdTrainingResultsId = 0;

    @BeforeMethod
    void init() {
        trainingResultsService = new TrainingResultsService();
        trainingResultsService.initClient(UserModel.TEST_USER);
    }

    @Test
    void postTrainingResultsTest() {
        TrainingResultsModel trainingResultsModel = new CustomResponse(trainingResultsService.
                postTrainingResults(TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainingResultsModel();
        createdTrainingResultsId = trainingResultsModel.getId();
        Assert.assertEquals(trainingResultsModel, TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL,
                "Training results created incorrectly!");
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        trainingResultsService.deleteTrainingResultsById(createdTrainingResultsId);
    }
}
