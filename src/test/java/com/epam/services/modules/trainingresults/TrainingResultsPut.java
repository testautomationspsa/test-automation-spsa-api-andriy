package com.epam.services.modules.trainingresults;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.TrainingResultsModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainingResultsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainingResultsPut extends BaseTest {
    private TrainingResultsService trainingResultsService;
    private Integer createdTrainingResultsId = 0;

    @BeforeMethod
    void init() {
        trainingResultsService = new TrainingResultsService();
        trainingResultsService.initClient(UserModel.TEST_USER);
        createdTrainingResultsId = new CustomResponse(trainingResultsService.
                postTrainingResults(TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL))
                .getResponseTrainingResultsModel().getId();
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "TrainingResultsData")
    void putTrainingResultsTest(String date, String result, String sportType,
                                String trainer, String venue, String responseStatus, String message) {
        new CustomResponse(trainingResultsService.
                putTrainingResults(createdTrainingResultsId, TrainingResultsModel.builder()
                        .date(date.equals("") ? null : date)
                        .result(result.equals("") ? null : Float.valueOf(result))
                        .sportType(sportType.equals("") ? null : sportType)
                        .trainer(trainer.equals("") ? null : trainer)
                        .venue(venue.equals("") ? null : venue).build()))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus))
                .verifyMessage(message);
        new CustomResponse(trainingResultsService.putTrainingResults(Integer.MAX_VALUE,
                TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        trainingResultsService.deleteTrainingResultsById(createdTrainingResultsId);
    }
}
