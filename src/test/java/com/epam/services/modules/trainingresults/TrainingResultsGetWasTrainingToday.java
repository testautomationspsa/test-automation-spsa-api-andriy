package com.epam.services.modules.trainingresults;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainingResultsModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainingResultsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TrainingResultsGetWasTrainingToday extends BaseTest {
    private TrainingResultsService trainingResultsService;
    private Integer createdTrainingResultsId = 0;

    @BeforeMethod
    void init() {
        trainingResultsService = new TrainingResultsService();
        trainingResultsService.initClient(UserModel.TEST_USER);
    }

    @Test
    void postTrainingResultsTest() {
        Boolean wasTrainingToday = new CustomResponse(trainingResultsService.
                getWasTrainingToday())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .getWasTrainingTodayValue();
        Assert.assertFalse(wasTrainingToday, "Didn't save my training results today, but result is true");
        ZoneId zonedId = ZoneId.of("Europe/Kiev");
        ZonedDateTime zdt = ZonedDateTime.now(zonedId);
        createdTrainingResultsId = postTrainingResult(TrainingResultsModel
                .CORRECT_TEST_TRAINING_RESULTS_MODEL.toBuilder()
                .date(zdt.format(DateTimeFormatter.ISO_LOCAL_DATE)).build());
        wasTrainingToday = new CustomResponse(trainingResultsService.
                getWasTrainingToday())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .getWasTrainingTodayValue();
        Assert.assertTrue(wasTrainingToday, "Did save my training results today, but result is false");
        trainingResultsService.deleteTrainingResultsById(createdTrainingResultsId);
        wasTrainingToday = new CustomResponse(trainingResultsService.
                getWasTrainingToday())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .getWasTrainingTodayValue();
        Assert.assertFalse(wasTrainingToday, "Wasn't training today, but result is true");
    }

    private Integer postTrainingResult(TrainingResultsModel trainingResultsModel) {
        return new CustomResponse(trainingResultsService.
                postTrainingResults(trainingResultsModel))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainingResultsModel().getId();
    }
}
