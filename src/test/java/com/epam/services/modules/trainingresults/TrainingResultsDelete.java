package com.epam.services.modules.trainingresults;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainingResultsModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainingResultsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainingResultsDelete extends BaseTest {
    private TrainingResultsService trainingResultsService;
    private Integer createdTrainingResultsId = 0;

    @BeforeMethod
    void init() {
        trainingResultsService = new TrainingResultsService();
        trainingResultsService.initClient(UserModel.TEST_USER);
        createdTrainingResultsId = new CustomResponse(trainingResultsService.
                postTrainingResults(TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainingResultsModel().getId();
    }

    @Test
    void deleteTrainingResultsTest() {
        new CustomResponse(trainingResultsService.deleteTrainingResultsById(createdTrainingResultsId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(trainingResultsService.deleteTrainingResultsById(createdTrainingResultsId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
