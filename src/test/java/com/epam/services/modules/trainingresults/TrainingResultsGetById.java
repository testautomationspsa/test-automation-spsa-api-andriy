package com.epam.services.modules.trainingresults;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainingResultsModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainingResultsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainingResultsGetById extends BaseTest {
    private TrainingResultsService trainingResultsService;
    private Integer createdTrainingResultsId = 0;

    @BeforeMethod
    void init() {
        trainingResultsService = new TrainingResultsService();
        trainingResultsService.initClient(UserModel.TEST_USER);
        createdTrainingResultsId = new CustomResponse(trainingResultsService.
                postTrainingResults(TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL))
                .getResponseTrainingResultsModel().getId();
    }

    @Test
    void getTrainingResultsByIdTest() {
        new CustomResponse(trainingResultsService.getTrainingResultsById(Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        TrainingResultsModel responseTrainingResultsModel = new CustomResponse(trainingResultsService
                .getTrainingResultsById(createdTrainingResultsId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseTrainingResultsModel();
        Assert.assertEquals(TrainingResultsModel.CORRECT_TEST_TRAINING_RESULTS_MODEL, responseTrainingResultsModel,
                "Created training-results and GET-request response are different!");
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        trainingResultsService.deleteTrainingResultsById(createdTrainingResultsId);
    }
}
