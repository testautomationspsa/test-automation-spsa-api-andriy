package com.epam.services.modules.events;

import com.epam.models.EventModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EventsDelete {
    private EventsService eventsService;
    private Integer createdEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        eventsService.initClient(UserModel.TEST_USER);
        createdEventId = new CustomResponse(eventsService
                .postEvent(EventModel.CORRECT_TEST_EVENT_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseEventResponseModel().getId();
    }

    @Test
    void deleteEvent() {
        new CustomResponse(eventsService.deleteByIdEvent(createdEventId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(eventsService.deleteByIdEvent(createdEventId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
