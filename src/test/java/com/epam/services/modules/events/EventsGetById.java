package com.epam.services.modules.events;

import com.epam.models.EventModel;
import com.epam.models.EventResponseModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EventsGetById {
    private EventsService eventsService;
    private Integer createdEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        eventsService.initClient(UserModel.TEST_USER);
        createdEventId = new CustomResponse(eventsService.postEvent(EventModel.CORRECT_TEST_EVENT_MODEL))
                .getResponseEventResponseModel().getId();
        eventsService.logOut();
    }

    @Test
    void getEventById() {
        eventsService.initClient(UserModel.TEST_USER);
        EventResponseModel eventResponseModel = new CustomResponse(eventsService.getByIdEvent(createdEventId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseEventResponseModel();
        Assert.assertTrue(EventModel.CORRECT_TEST_EVENT_MODEL.equalsToEventResponseModel(eventResponseModel),
                "Created event and GET-request response are different!");
        eventsService.deleteByIdEvent(createdEventId);
        new CustomResponse(eventsService.getByIdEvent(createdEventId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        eventsService.deleteByIdEvent(createdEventId);
    }
}
