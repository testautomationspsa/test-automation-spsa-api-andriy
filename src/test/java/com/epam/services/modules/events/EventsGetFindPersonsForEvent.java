package com.epam.services.modules.events;

import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EventsGetFindPersonsForEvent {
    private EventsService eventsService;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        eventsService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getFindPersonForEvent() {
        new CustomResponse(eventsService.getFindAllPersonsForEventEvents())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
