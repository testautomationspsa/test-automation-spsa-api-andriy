package com.epam.services.modules.events;

import com.epam.dataproviders.DataProviders;
import com.epam.models.EventModel;
import com.epam.models.EventResponseModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class EventsPut {
    private EventsService eventsService;
    private Integer createdEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        eventsService.initClient(UserModel.TEST_USER);
        createdEventId = new CustomResponse(eventsService.postEvent(EventModel.CORRECT_TEST_EVENT_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseEventResponseModel().getId();
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "eventsData")
    void putEvent(String title, String description, String beginTime, String endTime, String frequency, String responseStatus) {
        EventModel createdEvent = formEventModel(title, description, beginTime, endTime, frequency);
        CustomResponse customResponse = new CustomResponse(eventsService
                .putEvent(createdEventId, createdEvent))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        if (ResponseStatus.valueOf(responseStatus).equals(ResponseStatus.SUCCESS)) {
            EventResponseModel eventResponseModel = customResponse.getResponseEventResponseModel();
            Assert.assertTrue(createdEvent.equalsToEventResponseModel(eventResponseModel),
                    "Event update was not correct!");
        }
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        eventsService.deleteByIdEvent(createdEventId);
    }

    private EventModel formEventModel(String title, String description, String beginTime, String endTime, String frequency) {
        return EventModel.builder()
                .attendeesId(new ArrayList<>())
                .title(title)
                .description(description)
                .beginTime(beginTime)
                .endTime(endTime)
                .frequency(frequency).build();
    }
}
