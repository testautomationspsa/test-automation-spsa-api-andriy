package com.epam.services.modules.events;

import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class EventsGetAllByMonth {
    private EventsService eventsService;
    private Integer createdEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        eventsService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getAllEventsByMonth() {
        ZoneId zonedId = ZoneId.of("Europe/Kiev");
        String month = Month.of(ZonedDateTime.now(zonedId).getMonthValue()).toString();
        String year = Integer.toString(ZonedDateTime.now(zonedId).getYear());
        new CustomResponse(eventsService.getAllUserEventsByMonth(month, year))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseEventResponseModel();
        eventsService.deleteByIdEvent(createdEventId);
        new CustomResponse(eventsService.getByIdEvent(createdEventId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        eventsService.deleteByIdEvent(createdEventId);
    }
}
