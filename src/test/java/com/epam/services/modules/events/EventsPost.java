package com.epam.services.modules.events;

import com.epam.models.EventModel;
import com.epam.models.EventResponseModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EventsPost {
    private EventsService eventsService;
    private Integer createdEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        eventsService.initClient(UserModel.TEST_USER);
    }

    @Test
    void postEvent() {
        EventResponseModel eventResponseModel = new CustomResponse(eventsService
                .postEvent(EventModel.CORRECT_TEST_EVENT_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseEventResponseModel();
        createdEventId = eventResponseModel.getId();
        Assert.assertTrue(EventModel.CORRECT_TEST_EVENT_MODEL.equalsToEventResponseModel(eventResponseModel),
                "Event created incorrectly!");
        new CustomResponse(eventsService.getByIdEvent(createdEventId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseEventResponseModel();
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        eventsService.deleteByIdEvent(createdEventId);
    }
}
