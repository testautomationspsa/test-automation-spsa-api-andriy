package com.epam.services.modules.post;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.PostModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.PostService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PostPut extends BaseTest {
    private PostService postService;
    private Integer createdPostId = 0;

    @BeforeMethod
    void init() {
        postService = new PostService();
        postService.initClient(UserModel.TEST_SUPER_ADMIN);
        createdPostId = new CustomResponse(postService.postPost(PostModel.CORRECT_TEST_POST_MODEL))
                .getResponsePostModel().getId();
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "postData")
    void putPostTest(String name, String text, String tag, String responseStatus) {
        PostModel postModel =
                PostModel.CORRECT_TEST_POST_MODEL.toBuilder()
                        .name(name)
                        .text(text)
                        .tags(new String[]{tag}).build();
        new CustomResponse(postService.putPost(postModel, createdPostId))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus));
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        postService.deleteByIdPost(createdPostId);
    }
}
