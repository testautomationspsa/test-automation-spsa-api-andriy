package com.epam.services.modules.post;

import com.epam.fortest.BaseTest;
import com.epam.models.PostModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.PostService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PostPostCreate extends BaseTest {
    private PostService postService;
    private Integer createdPostId = 0;

    @BeforeMethod
    void init() {
        postService = new PostService();
        postService.initClient(UserModel.TEST_SUPER_ADMIN);
    }

    @Test
    void addPostTest() {
        PostModel postModel = new CustomResponse(postService.postPost(PostModel.CORRECT_TEST_POST_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponsePostModel();
        createdPostId = postModel.getId();
        Assert.assertEquals(PostModel.CORRECT_TEST_POST_MODEL, postModel, "Post created incorrectly!");
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        postService.deleteByIdPost(createdPostId);
    }
}
