package com.epam.services.modules.post;

import com.epam.fortest.BaseTest;
import com.epam.models.PostModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.PostService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PostGetById extends BaseTest {
    private PostService postService;
    private Integer createdPostId = 0;

    @BeforeMethod
    void init() {
        postService = new PostService();
        postService.initClient(UserModel.TEST_SUPER_ADMIN);
        createdPostId = new CustomResponse(postService.postPost(PostModel.CORRECT_TEST_POST_MODEL))
                .getResponsePostModel().getId();
    }

    @Test
    void getPostById() {
        PostModel resposePostModel = new CustomResponse(postService.getPostById(createdPostId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponsePostModel();
        Assert.assertEquals(PostModel.CORRECT_TEST_POST_MODEL, resposePostModel,
                "Created post and GET-request response are different!");
        postService.deleteByIdPost(createdPostId);
        new CustomResponse(postService.getPostById(createdPostId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        postService.deleteByIdPost(createdPostId);
    }
}
