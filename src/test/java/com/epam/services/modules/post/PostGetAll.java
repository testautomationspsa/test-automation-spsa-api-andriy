package com.epam.services.modules.post;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.PostService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PostGetAll extends BaseTest {
    private PostService postService;

    @BeforeMethod
    void init() {
        postService = new PostService();
        postService.initClient(UserModel.TEST_SUPER_ADMIN);
    }

    @Test()
    void getAllPosts() {
        new CustomResponse(postService.getAllPosts())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
