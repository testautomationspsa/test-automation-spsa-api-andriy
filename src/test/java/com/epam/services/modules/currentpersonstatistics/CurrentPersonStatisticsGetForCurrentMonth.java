package com.epam.services.modules.currentpersonstatistics;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.CurrentPersonStatisticsService;
import com.epam.utils.enums.ResponseStatus;
import com.epam.utils.enums.SportType;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

public class CurrentPersonStatisticsGetForCurrentMonth extends BaseTest {
    private CurrentPersonStatisticsService cps = new CurrentPersonStatisticsService();

    @BeforeMethod
    void init() {
        cps = new CurrentPersonStatisticsService();
        cps.initClient(UserModel.TEST_USER);
    }

    @Test
    void getCurrentPersonStatisticsGetForCurrentMonth() {
        Arrays.asList(SportType.values()).forEach(
                sportType -> {
                    SportType responseSportType = SportType.valueOf(new CustomResponse(
                            cps.getCurrentPersonStatisticsForMonthCurrentTest(sportType.toString()))
                            .verifyStatusCode(ResponseStatus.SUCCESS)
                            .verifyStatus(ResponseStatus.SUCCESS)
                            .getResponseUserTrainingStatisticsModel()
                            .getSportType());
                    Assert.assertEquals(responseSportType, sportType);
                });
    }
}
