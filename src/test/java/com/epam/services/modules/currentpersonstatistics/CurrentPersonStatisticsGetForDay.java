package com.epam.services.modules.currentpersonstatistics;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.CurrentPersonStatisticsService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import com.epam.utils.enums.SportType;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

public class CurrentPersonStatisticsGetForDay extends BaseTest {
    private CurrentPersonStatisticsService cps = new CurrentPersonStatisticsService();

    @BeforeMethod
    void init() {
        cps = new CurrentPersonStatisticsService();
        cps.initClient(UserModel.TEST_USER);
    }

    @Test
    void getCurrentPersonStatisticsForDay() {
        Arrays.asList(SportType.values()).forEach(
                sportType -> {
                    SportType responseSportType = SportType.valueOf(new CustomResponse(
                            cps.getCurrentPersonStatisticsForDayTest(sportType.toString()))
                            .verifyStatusCode(ResponseStatus.SUCCESS)
                            .verifyStatus(ResponseStatus.SUCCESS)
                            .getResponseUserTrainingStatisticsModel()
                            .getSportType());
                    Assert.assertEquals(responseSportType, sportType);
                });
        new CustomResponse(
                cps.getCurrentPersonStatisticsForDayTest("IncorrectSportType"))
                .verifyStatusCode(ResponseStatus.BAD_REQUEST)
                .verifyStatus(ResponseStatus.BAD_REQUEST)
                .verifyMessage(Constants.WRONG_SPORT_TYPE_EXCEPTION);
    }
}
