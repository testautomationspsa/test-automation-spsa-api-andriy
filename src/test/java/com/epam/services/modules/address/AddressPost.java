package com.epam.services.modules.address;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.models.submodels.AddressModel;
import com.epam.services.AddressService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AddressPost extends BaseTest {
    private AddressService addressService;

    @BeforeMethod
    void init() {
        addressService = new AddressService();
        addressService.initClient(UserModel.TEST_USER);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "addressData")
    void postAddressTest(String country, String city, String street, String streetNumber,
                         String latitude, String longitude, String responseStatus) {
        AddressModel addressModel = formAddressModel(country, city, street, streetNumber, latitude, longitude);
        new CustomResponse(addressService.postAddress(addressModel))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus));
    }

    private AddressModel formAddressModel(String country, String city, String street, String streetNumber, String latitude, String longitude) {
        return AddressModel.builder()
                .city(city.equals("") ? null : city)
                .country(country.equals("") ? null : country)
                .street(street.equals("") ? null : street)
                .streetNumber(streetNumber.equals("") ? null : streetNumber)
                .latitude(Double.valueOf(latitude))
                .longitude(Double.valueOf(longitude)).build();
    }

    @AfterClass(alwaysRun = true)
    void restoreDefaultAddress() {
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
    }
}
