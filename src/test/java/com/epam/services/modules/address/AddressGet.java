package com.epam.services.modules.address;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.models.submodels.AddressModel;
import com.epam.services.AddressService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AddressGet extends BaseTest {
    private AddressService addressService;

    @BeforeMethod
    void init() {
        addressService = new AddressService();
        addressService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getAddressTest() {
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
        AddressModel responseAddressModel = new CustomResponse(addressService.getAddress())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseAddressModel();
        Assert.assertEquals(responseAddressModel, AddressModel.CORRECT_TEST_ADDRESS_MODEL,
                "Created address and GET-request response are different!");
    }
}
