package com.epam.services.modules.address;

import com.epam.fortest.BaseTest;
import com.epam.models.response.CustomResponse;
import com.epam.models.UserModel;
import com.epam.models.submodels.AddressModel;
import com.epam.services.AddressService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AddressDelete extends BaseTest {
    private AddressService addressService;

    @BeforeMethod
    void init(){
        addressService = new AddressService();
        addressService.initClient(UserModel.TEST_USER);
    }

    @Test
    void deleteAddressTest(){
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
        new CustomResponse(addressService.deleteAddress())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(addressService.deleteAddress())
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
