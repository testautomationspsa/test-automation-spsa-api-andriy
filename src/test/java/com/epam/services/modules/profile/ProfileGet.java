package com.epam.services.modules.profile;

import com.epam.fortest.BaseTest;
import com.epam.models.ProfileModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.ProfileService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ProfileGet extends BaseTest {
    private ProfileService profileService;

    @BeforeMethod
    void init() {
        profileService = new ProfileService();
        profileService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getAddressTest() {
        profileService.postProfile(ProfileModel.CORRECT_TEST_PROFILE_MODEL);
        ProfileModel responseProfileModel = new CustomResponse(profileService.getProfile())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseProfileModel();
        Assert.assertEquals(responseProfileModel, ProfileModel.CORRECT_TEST_PROFILE_MODEL,
                "Created profile and GET-request response are different!");
    }
}
