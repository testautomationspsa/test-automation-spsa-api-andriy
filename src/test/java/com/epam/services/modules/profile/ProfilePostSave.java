package com.epam.services.modules.profile;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.ProfileModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.models.submodels.AddressModel;
import com.epam.services.ProfileService;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Objects;


public class ProfilePostSave extends BaseTest {
    private ProfileService profileService;

    @BeforeMethod
    void init() {
        profileService = new ProfileService();
        profileService.initClient(UserModel.TEST_USER);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "saveProfileData")
    void saveProfileTest(String country, String city, String street, String streetNumber, String latitude, String longitude,
                         String dateOfBirth, String firstName, String secondName, String gender, String fileName, String responseStatus) {
        ProfileModel testProfile = formProfileModel(country, city, street, streetNumber, latitude, longitude, dateOfBirth, firstName, secondName, gender, fileName);
        ProfileModel responseProfileModel = new CustomResponse(profileService.postProfile(testProfile))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus)).getResponseProfileModel();
        if (Objects.nonNull(responseProfileModel)) {
            testProfile.setGender(testProfile.getGender().equals("1") ?
                    Gender.FEMALE.toString() : testProfile.getGender());
            Assert.assertEquals(testProfile, responseProfileModel, "Profile created incorrectly!");
        }

    }

    private ProfileModel formProfileModel(String country, String city, String street, String streetNumber,
                                          String latitude, String longitude, String dateOfBirth, String firstName,
                                          String secondName, String gender, String fileName) {
        AddressModel addressModel =
                AddressModel.builder()
                        .city(city)
                        .country(country)
                        .street(street)
                        .streetNumber(streetNumber)
                        .latitude(Double.valueOf(latitude))
                        .longitude(Double.valueOf(longitude)).build();
        return ProfileModel.builder()
                .address(addressModel)
                .dateOfBirthday(dateOfBirth)
                .email(UserModel.TEST_USER.getEmail())
                .firstName(firstName)
                .secondName(secondName)
                .gender(gender)
                .verification("true")
                .fileName(fileName).build();
    }

    @AfterClass(alwaysRun = true)
    void restoreDefaultAddress() {
        profileService.postProfile(ProfileModel.CORRECT_TEST_PROFILE_MODEL);
    }
}
