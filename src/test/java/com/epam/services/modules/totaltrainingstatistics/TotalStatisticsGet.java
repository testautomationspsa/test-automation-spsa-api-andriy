package com.epam.services.modules.totaltrainingstatistics;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TotalStatisticsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TotalStatisticsGet extends BaseTest {
    private TotalStatisticsService totalStatisticsService;

    @BeforeMethod
    void init() {
        totalStatisticsService = new TotalStatisticsService();
        totalStatisticsService.initClient(UserModel.TEST_USER);
    }

    @Test()
    void getTotalTrainingStatistics() {
        int totalUsers = new CustomResponse(totalStatisticsService.getTotalTrainingStatistics())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseTotalTrainingStatisticsModel().getTotalUsers();
        Assert.assertTrue(totalUsers > 0, "Incorrect Application State");
    }
}
