package com.epam.services.modules.approveevent;

import com.epam.models.ApprovedEventModel;
import com.epam.models.EventModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.ApproveEventService;
import com.epam.services.EventsService;
import com.epam.utils.enums.Confirmation;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ApproveEventPutApprove {
    private EventsService eventsService;
    private ApproveEventService approveEventService;
    private Integer createdEventId = 0;
    private Integer createdApprovedEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        approveEventService = new ApproveEventService();
        eventsService.initClient(UserModel.TEST_USER);
        createdEventId = new CustomResponse(eventsService.postEvent(EventModel.CORRECT_TEST_EVENT_MODEL))
                .getResponseEventResponseModel().getId();
        eventsService.logOut();
        approveEventService.initClient(UserModel.TEST_TRAINER);
    }

    @Test
    void approveEventById() {
        ApprovedEventModel approvedEventModel = new CustomResponse(approveEventService
                .putApproveEvent(createdEventId, true))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseApprovedEventModel();
        createdApprovedEventId = approvedEventModel.getId();
        Assert.assertEquals(Confirmation.CONFIRMED.toString(), approvedEventModel.getConfirmation(),
                "Event was not approved correctly!");
        approveEventService.logOut();
        approveEventService.initClient(UserModel.TEST_TRAINER);
        approvedEventModel = new CustomResponse(approveEventService
                .putApproveEvent(createdEventId, false))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseApprovedEventModel();
        Assert.assertEquals(Confirmation.NOT_CONFIRMED.toString(), approvedEventModel.getConfirmation(),
                "Event was not approved correctly!");
        new CustomResponse(approveEventService.getByIdApprovedEvent(approvedEventModel.getId()))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        approveEventService.logOut();
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        eventsService.initClient(UserModel.TEST_USER);
        eventsService.deleteByIdEvent(createdApprovedEventId);
        eventsService.logOut();
    }
}
