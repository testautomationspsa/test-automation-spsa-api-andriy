package com.epam.services.modules.approveevent;

import com.epam.models.ApprovedEventModel;
import com.epam.models.EventModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.ApproveEventService;
import com.epam.services.EventsService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ApproveEventDelete {
    private EventsService eventsService;
    private ApproveEventService approveEventService;
    private Integer createdEventId = 0;

    @BeforeMethod
    void init() {
        eventsService = new EventsService();
        approveEventService = new ApproveEventService();
        eventsService.initClient(UserModel.TEST_USER);
        createdEventId = new CustomResponse(eventsService.postEvent(EventModel.CORRECT_TEST_EVENT_MODEL))
                .getResponseEventResponseModel().getId();
        eventsService.logOut();
        approveEventService.initClient(UserModel.TEST_TRAINER);
    }

    @Test
    void deleteEventById() {
        ApprovedEventModel approvedEventModel = new CustomResponse(approveEventService
                .putApproveEvent(createdEventId, true))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseApprovedEventModel();
        new CustomResponse(approveEventService.deleteByIdApprovedEvent(approvedEventModel.getId()))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(approveEventService.getByIdApprovedEvent(approvedEventModel.getId()))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        approveEventService.logOut();
    }
}
