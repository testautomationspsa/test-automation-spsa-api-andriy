package com.epam.services.modules.personinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.PersonInfoModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.PersonInfoService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PersonInfoGet extends BaseTest {
    private PersonInfoService personInfoService;

    @BeforeMethod
    void init() {
        personInfoService = new PersonInfoService();
        personInfoService.initClient(UserModel.TEST_SUPER_ADMIN);
        personInfoService.postPersonInfo(PersonInfoModel.CORRECT_PERSON_INFO_MODEL);
    }

    @Test()
    void getPersonInfoTest() {
        new CustomResponse(personInfoService.getPersonInfo())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponsePersonInfoModel();
        personInfoService.deletePersonInfo();
        new CustomResponse(personInfoService.getPersonInfo())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
