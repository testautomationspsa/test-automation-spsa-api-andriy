package com.epam.services.modules.personinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.PersonInfoModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.PersonInfoService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PersonInfoPost extends BaseTest {
    private PersonInfoService personInfoService;

    @BeforeMethod
    void init() {
        personInfoService = new PersonInfoService();
        personInfoService.initClient(UserModel.TEST_SUPER_ADMIN);
    }

    @Test
    void postPersonInfoTest() {
        PersonInfoModel personInfoModel = new CustomResponse(personInfoService.postPersonInfo(
                PersonInfoModel.CORRECT_PERSON_INFO_MODEL))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponsePersonInfoModel();
        Assert.assertEquals(PersonInfoModel.CORRECT_PERSON_INFO_MODEL, personInfoModel,
                "Person info created incorrectly!");
    }
}
