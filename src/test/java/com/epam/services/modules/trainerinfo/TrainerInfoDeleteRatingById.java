package com.epam.services.modules.trainerinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainerFeedbackModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerInfoDeleteRatingById extends BaseTest {
    private TrainerInfoService trainerInfoService;
    private Integer createdRatingId = 0;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerInfoService.initClient(UserModel.TEST_USER);
        createdRatingId = new CustomResponse(trainerInfoService
                .postTrainerInfoRating(Constants.TEST_TRAINER_INFO_ID, TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_1))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainerFeedbackModel().getId();
    }

    @Test
    void deleteTrainerInfoRatingTest() {
        new CustomResponse(trainerInfoService.deleteTrainerInfoRating(createdRatingId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(trainerInfoService.deleteTrainerInfoRating(createdRatingId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
