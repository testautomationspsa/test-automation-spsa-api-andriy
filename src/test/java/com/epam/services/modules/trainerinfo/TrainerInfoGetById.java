package com.epam.services.modules.trainerinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainerInfoModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerInfoGetById extends BaseTest {
    private TrainerInfoService trainerInfoService;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerInfoService.initClient(UserModel.TEST_TRAINER);
        trainerInfoService.postTrainerInfo(TrainerInfoModel.CORRECT_TRAINER_INFO_MODEL);
        trainerInfoService.logOut();
        trainerInfoService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getTrainerInfoByIdTest() {
        TrainerInfoModel responseTrainerInfoModel = new CustomResponse(trainerInfoService
                .getByIdTrainerInfo(Constants.TEST_TRAINER_INFO_ID))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseTrainerInfoModel();
        Assert.assertEquals(TrainerInfoModel.CORRECT_TRAINER_INFO_MODEL, responseTrainerInfoModel,
                "Created trainer-info and GET-request response are different!");
        new CustomResponse(trainerInfoService.getByIdTrainerInfo(Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
