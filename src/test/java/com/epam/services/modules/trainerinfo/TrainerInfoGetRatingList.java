package com.epam.services.modules.trainerinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerInfoGetRatingList extends BaseTest {
    private TrainerInfoService trainerInfoService;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerInfoService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getTrainerInfoRatingListTest() {
        new CustomResponse(trainerInfoService.getTrainerInfoRatingList(Constants.TEST_TRAINER_INFO_ID))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(trainerInfoService.getTrainerInfoRatingList(Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
    }
}
