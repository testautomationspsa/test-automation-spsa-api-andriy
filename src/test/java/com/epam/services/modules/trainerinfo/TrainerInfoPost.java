package com.epam.services.modules.trainerinfo;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.TrainerInfoModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Objects;

public class TrainerInfoPost extends BaseTest {
    private TrainerInfoService trainerInfoService;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerInfoService.initClient(UserModel.TEST_TRAINER);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "trainerInfoData")
    void postTrainerInfoTest(String experience, String sportType, String phoneNumber,
                             String responseStatus) {
        TrainerInfoModel trainerInfoModel = TrainerInfoModel.builder()
                .experience(experience.equals("") ? null : Integer.valueOf(experience))
                .sportType(sportType.equals("") ? null : sportType)
                .phoneNumber(phoneNumber.equals("") ? null : phoneNumber)
                .build();
        TrainerInfoModel responseTrainerInfoModel = new CustomResponse(trainerInfoService
                .postTrainerInfo(trainerInfoModel))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus))
                .getResponseTrainerInfoModel();
        if (Objects.nonNull(responseTrainerInfoModel)) {
            Assert.assertEquals(trainerInfoModel, responseTrainerInfoModel,
                    "Trainer-info created incorrectly!");
        }
    }

    @AfterClass
    void restoreDefault() {
        trainerInfoService.postTrainerInfo(TrainerInfoModel.CORRECT_TRAINER_INFO_MODEL);
    }
}
