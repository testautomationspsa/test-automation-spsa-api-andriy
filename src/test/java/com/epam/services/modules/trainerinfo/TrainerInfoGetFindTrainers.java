package com.epam.services.modules.trainerinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.FindTrainerModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.ResponseStatus;
import com.epam.utils.enums.SportType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerInfoGetFindTrainers extends BaseTest {
    private static final String GENDER_IS_NULL_ERROR_MESSAGE = "[gender: Value cannot be null]";
    private static final String SPORT_TYPE_IS_NULL_ERROR_MESSAGE = "[sportType: Value cannot be null]";
    private TrainerInfoService trainerInfoService;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerInfoService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getTrainerInfoRatingListTest() {
        new CustomResponse(trainerInfoService.getFindTrainer(
                new FindTrainerModel(null, null, SportType.RUNNING.toString(), null)))
                .verifyStatusCode(ResponseStatus.VALIDATION_MISTAKE)
                .verifyStatus(ResponseStatus.VALIDATION_MISTAKE)
                .verifyMessage(GENDER_IS_NULL_ERROR_MESSAGE);
        trainerInfoService.logOut();
        trainerInfoService.initClient(UserModel.TEST_USER);
        new CustomResponse(trainerInfoService.getFindTrainer(
                new FindTrainerModel(null, Gender.ANY.toString(), null, null)))
                .verifyStatusCode(ResponseStatus.VALIDATION_MISTAKE)
                .verifyStatus(ResponseStatus.VALIDATION_MISTAKE)
                .verifyMessage(SPORT_TYPE_IS_NULL_ERROR_MESSAGE);
        trainerInfoService.logOut();
        trainerInfoService.initClient(UserModel.TEST_USER);
        new CustomResponse(trainerInfoService.getFindTrainer(
                new FindTrainerModel(null, Gender.ANY.toString(), SportType.RUNNING.toString(), null)))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        trainerInfoService.logOut();
    }
}
