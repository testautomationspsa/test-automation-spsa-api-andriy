package com.epam.services.modules.trainerinfo;

import com.epam.fortest.BaseTest;
import com.epam.models.TrainerFeedbackModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.TrainerInfoService;
import com.epam.utils.Constants;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TrainerInfoPutRating extends BaseTest {
    private final String CAN_NOT_UPDATE_NOT_YOURS_EXCEPTION = "This feedback is not yours";
    private TrainerInfoService trainerInfoService;
    private Integer createdRatingId = 0;

    @BeforeMethod
    void init() {
        trainerInfoService = new TrainerInfoService();
        trainerInfoService.initClient(UserModel.TEST_USER);
        createdRatingId = new CustomResponse(trainerInfoService
                .postTrainerInfoRating(Constants.TEST_TRAINER_INFO_ID, TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_1))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseTrainerFeedbackModel().getId();
    }

    @Test
    void putTrainerInfoRatingTest() {
        TrainerFeedbackModel trainerFeedbackModel = new CustomResponse(trainerInfoService
                .putTrainerInfoRating(createdRatingId, TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_2))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseTrainerFeedbackModel();
        Assert.assertEquals(TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_2.getComment(),
                trainerFeedbackModel.getComment());
        Assert.assertEquals(TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_2.getEstimation(),
                trainerFeedbackModel.getEstimation());
        new CustomResponse(trainerInfoService
                .putTrainerInfoRating(Integer.MAX_VALUE, TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_1))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
        trainerInfoService.initClient(UserModel.TEST_TRAINER);
        new CustomResponse(trainerInfoService
                .putTrainerInfoRating(createdRatingId, TrainerFeedbackModel.CORRECT_TRAINER_FEEDBACK_MODEL_RATE_3))
                .verifyStatusCode(ResponseStatus.BAD_REQUEST)
                .verifyStatus(ResponseStatus.BAD_REQUEST)
                .verifyMessage(CAN_NOT_UPDATE_NOT_YOURS_EXCEPTION);
    }

    @AfterClass(alwaysRun = true)
    void clean() {
        trainerInfoService.deleteTrainerInfoRating(createdRatingId);
    }
}
