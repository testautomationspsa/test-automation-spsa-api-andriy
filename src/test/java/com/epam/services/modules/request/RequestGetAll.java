package com.epam.services.modules.request;

import com.epam.fortest.BaseTest;
import com.epam.models.response.CustomResponse;
import com.epam.models.RequestModel;
import com.epam.models.UserModel;
import com.epam.services.RequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestGetAll extends BaseTest {
    private RequestService requestService;

    @BeforeMethod
    void init() {
        requestService = new RequestService();
        requestService.initClient(UserModel.TEST_USER);
        requestService.postRequest(RequestModel.CORRECT_TEST_REQUEST_MODEL);
    }

    @Test
    void getAllRequestsTest() {
        new CustomResponse(requestService.getAllRequest())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        requestService.deleteAllRequest();
        new CustomResponse(requestService.getAllRequest())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);

    }
}
