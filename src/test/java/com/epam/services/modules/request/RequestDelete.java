package com.epam.services.modules.request;

import com.epam.fortest.BaseTest;
import com.epam.models.response.CustomResponse;
import com.epam.models.RequestModel;
import com.epam.models.UserModel;
import com.epam.services.RequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestDelete extends BaseTest {
    private RequestService requestService;
    private Integer createdRequestId = 0;

    @BeforeMethod
    void init() {
        requestService = new RequestService();
        requestService.initClient(UserModel.TEST_USER);
        createdRequestId = new CustomResponse(requestService.postRequest(RequestModel.CORRECT_TEST_REQUEST_MODEL))
                .getResponseRequestModel().getId();
    }

    @Test
    void deleteRequestByIdTest() {
        new CustomResponse(requestService.deleteByIdRequest(createdRequestId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        new CustomResponse(requestService.deleteByIdRequest(createdRequestId))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }
}
