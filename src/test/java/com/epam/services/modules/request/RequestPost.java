package com.epam.services.modules.request;

import com.epam.fortest.BaseTest;
import com.epam.models.RequestModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.RequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestPost extends BaseTest {
    private RequestService requestService;
    private Integer createdRequestId = 0;

    @BeforeMethod
    void init() {
        requestService = new RequestService();
        requestService.initClient(UserModel.TEST_USER);
    }

    @Test
    void postRequestTest() {
        RequestModel requestModelResponse = new CustomResponse(requestService.postRequest(RequestModel.CORRECT_TEST_REQUEST_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseRequestModel();
        Assert.assertEquals(RequestModel.CORRECT_TEST_REQUEST_MODEL, requestModelResponse,
                "Request was not created correctly!");
        createdRequestId = requestModelResponse.getId();
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        requestService.deleteByIdRequest(createdRequestId);
    }
}
