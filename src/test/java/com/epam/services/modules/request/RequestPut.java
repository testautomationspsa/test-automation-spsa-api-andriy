package com.epam.services.modules.request;

import com.epam.fortest.BaseTest;
import com.epam.models.RequestModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.RequestService;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestPut extends BaseTest {
    private RequestService requestService;
    private Integer createdRequestId = 0;

    @BeforeMethod
    void init() {
        requestService = new RequestService();
        requestService.initClient(UserModel.TEST_USER);
        createdRequestId = new CustomResponse(requestService.postRequest(RequestModel.CORRECT_TEST_REQUEST_MODEL))
                .verifyStatusCode(ResponseStatus.CREATED)
                .verifyStatus(ResponseStatus.CREATED)
                .getResponseRequestModel().getId();
    }

    @Test
    void putRequestTest() {
        RequestModel editedRequest = RequestModel.CORRECT_TEST_REQUEST_MODEL.toBuilder()
                .gender(Gender.MALE).build();
        RequestModel editedRequestResponse = new CustomResponse(requestService
                .putRequest(createdRequestId, editedRequest))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseRequestModel();
        Assert.assertEquals(editedRequest, editedRequestResponse, "Request was not edited correctly!");
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        requestService.deleteByIdRequest(createdRequestId);
    }
}
