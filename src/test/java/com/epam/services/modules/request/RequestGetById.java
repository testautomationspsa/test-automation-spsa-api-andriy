package com.epam.services.modules.request;

import com.epam.fortest.BaseTest;
import com.epam.models.RequestModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.services.RequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestGetById extends BaseTest {
    private RequestService requestService;
    private Integer createdRequestId = 0;

    @BeforeMethod
    void init() {
        requestService = new RequestService();
        requestService.initClient(UserModel.TEST_USER);
        createdRequestId = new CustomResponse(requestService.postRequest(RequestModel.CORRECT_TEST_REQUEST_MODEL))
                .getResponseRequestModel().getId();
    }

    @Test
    void getRequestByIdTest() {
        RequestModel responseRequestModel = new CustomResponse(requestService.getByIdRequest(createdRequestId))
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS)
                .getResponseRequestModel();
        Assert.assertEquals(RequestModel.CORRECT_TEST_REQUEST_MODEL, responseRequestModel,
                "Created request and GET-request response are different!");
        new CustomResponse(requestService.getByIdRequest(Integer.MAX_VALUE))
                .verifyStatusCode(ResponseStatus.NOT_FOUND)
                .verifyStatus(ResponseStatus.NOT_FOUND);
    }

    @AfterMethod(alwaysRun = true)
    void clean() {
        requestService.deleteByIdRequest(createdRequestId);
    }
}
