package com.epam.services.modules.request;

import com.epam.dataproviders.DataProviders;
import com.epam.fortest.BaseTest;
import com.epam.models.PartnerModel;
import com.epam.models.RequestModel;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.models.submodels.AddressModel;
import com.epam.services.AddressService;
import com.epam.services.RequestService;
import com.epam.utils.Constants;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.ResponseStatus;
import com.epam.utils.enums.SportType;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class RequestGetFindPartners extends BaseTest {
    private RequestService requestService;
    private AddressService addressService;

    @BeforeMethod
    void init() {
        addressService = new AddressService();
        requestService = new RequestService();
        addressService.initClient(UserModel.TEST_USER);
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
        requestService.initClient(UserModel.TEST_USER);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "findPartnersValidationData")
    void findPartnersValidationTest(String distance, String minRunningDistance, String maxRunningDistance,
                          String morning, String afternoon, String evening,
                          String gender, String maturityLevel, String sportType, String responseStatus, String responseMessage) {
        RequestModel testRequest = formRequestModel(distance, minRunningDistance, maxRunningDistance,
                morning, afternoon, evening, gender, maturityLevel, sportType);
        new CustomResponse(requestService.findPartnersByRequest(testRequest))
                .verifyStatusCode(ResponseStatus.valueOf(responseStatus))
                .verifyStatus(ResponseStatus.valueOf(responseStatus))
                .verifyMessage(responseMessage);
        addressService.deleteAddress();
        new CustomResponse(requestService.findPartnersByRequest(testRequest))
                .verifyStatusCode(responseStatus.equals("VALIDATION_MISTAKE") ?
                        ResponseStatus.VALIDATION_MISTAKE : ResponseStatus.BAD_REQUEST_REQUEST_FP_NO_ADDRESS)
                .verifyStatus(responseStatus.equals("VALIDATION_MISTAKE") ?
                        ResponseStatus.VALIDATION_MISTAKE : ResponseStatus.BAD_REQUEST_REQUEST_FP_NO_ADDRESS)
                .verifyMessage(responseStatus.equals("VALIDATION_MISTAKE") ?
                        responseMessage : Constants.PERSON_HAS_NO_ADDRESS_EXCEPTION);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "findPartnersData")
    void findPartnersTest(String distance, String minRunningDistance, String maxRunningDistance,
                          String morning, String afternoon, String evening,
                          String gender, String maturityLevel, String sportType) {
        RequestModel testRequest = formRequestModel(distance, minRunningDistance, maxRunningDistance,
                morning, afternoon, evening, gender, maturityLevel, sportType);
        List<PartnerModel> partners =  new CustomResponse(requestService.findPartnersByRequest(testRequest))
                .getLongResponsePartnerModelList();
        partners.forEach(partner -> {Assert.assertEquals(maturityLevel,partner.getMaturityLevel(),"Maturity level mismatch!");
        Assert.assertEquals(sportType,partner.getSportType(),"Sport Type mismatch!");
        Assert.assertTrue(gender.equals("ANY") || gender.equals(partner.getPartnerGender()),"Gender mismatch!");
        Assert.assertTrue(Boolean.valueOf(morning)==partner.getMorning()||
                Boolean.valueOf(afternoon)==partner.getAfternoon() ||
                Boolean.valueOf(evening)==partner.getEvening(),"Training Time mismatch!");
    });
    }

    private RequestModel formRequestModel(String distance, String minRunningDistance, String maxRunningDistance, String morning, String afternoon, String evening, String gender, String maturityLevel, String sportType) {
        return RequestModel.builder()
                .afternoon(afternoon.equals("") ? null : Boolean.valueOf(afternoon))
                .distance(distance.equals("") ? null : Integer.valueOf(distance))
                .evening(evening.equals("") ? null : Boolean.valueOf(evening))
                .gender(gender.equals("") ? null : Gender.valueOf(gender))
                .maturityLevel(maturityLevel.equals("") ? null : MaturityLevel.valueOf(maturityLevel))
                .morning(morning.equals("") ? null : Boolean.valueOf(morning))
                .minRunningDistance(minRunningDistance.equals("") ? null : Integer.valueOf(minRunningDistance))
                .maxRunningDistance(maxRunningDistance.equals("") ? null : Integer.valueOf(maxRunningDistance))
                .sportType(sportType.equals("") ? null : SportType.valueOf(sportType)).build();
    }

    @AfterMethod(alwaysRun = true)
    void restoreAddress() {
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
    }
}
