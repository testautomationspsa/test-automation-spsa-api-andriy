package com.epam.services.modules.request;

import com.epam.fortest.BaseTest;
import com.epam.models.UserModel;
import com.epam.models.response.CustomResponse;
import com.epam.models.submodels.AddressModel;
import com.epam.services.AddressService;
import com.epam.services.RequestService;
import com.epam.utils.enums.ResponseStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RequestGetPossiblePartners extends BaseTest {
    private RequestService requestService;
    private AddressService addressService;

    @BeforeMethod
    void init() {
        addressService = new AddressService();
        requestService = new RequestService();
        addressService.initClient(UserModel.TEST_USER);
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
        requestService.initClient(UserModel.TEST_USER);
    }

    @Test
    void getAllRequestsTest() {
        new CustomResponse(requestService.getPossiblePartnersRequest())
                .verifyStatusCode(ResponseStatus.SUCCESS)
                .verifyStatus(ResponseStatus.SUCCESS);
        addressService.deleteAddress();
        new CustomResponse(requestService.getPossiblePartnersRequest())
                .verifyStatusCode(ResponseStatus.BAD_REQUEST_REQUEST_FP_NO_ADDRESS)
                .verifyStatus(ResponseStatus.BAD_REQUEST_REQUEST_FP_NO_ADDRESS);

    }

    @AfterMethod(alwaysRun = true)
    void restoreAddress() {
        addressService.postAddress(AddressModel.CORRECT_TEST_ADDRESS_MODEL);
    }
}
